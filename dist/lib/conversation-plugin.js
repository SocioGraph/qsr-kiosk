
var playBeep = (function beep() {
    var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
    return function() {
        snd.play();
    }
})();
var _empty_audio_played = false;
var playEmptyAudio= (function empty(){
    var snd = new Audio("data:audio/wav;base64,UklGRjIAAABXQVZFZm10IBIAAAABAAEAQB8AAEAfAAABAAgAAABmYWN0BAAAAAAAAABkYXRhAAAAAA==");
    return function(){
        if(_empty_audio_played) return;
        snd.play();
        _empty_audio_played=true
    }
})();
$(document).ready(function(){
    $(window).click(function(){
        playEmptyAudio();
    })
})
function set_cc(noresize) {
    var x = window.innerWidth;
    let canvas = $("#unityContainer");
    if(window.innerWidth < 767){
        canvas.css("max-height", "50%");
        let s = $( "#unityContainerP" ).children().first().offset()['top'] - $( ".dave-chat").offset()['top'] ;
        $( ".chat-header").css("top", s + "px")
        $( ".dave-whiteboard").css("height", s+"px");
        $( ".options-panel.image img").css("height", s+"px");
        canvas.css("width", 2*x + "px");
        canvas.css("height", x + "px");
        // canvas.css("bottom", "67px");
        canvas.css("right", "-35%");
        canvas.css("position", "absolute");
    } else {
        $( ".chat-header").css("top", "unset")
        canvas.css("width", "100%");
        canvas.css("height", "100%");
    }
    if ( (!noresize) && set_cc.resize && Array.isArray(set_cc.resize) )  {
        reset_size.apply({}, set_cc.resize)
    }
}
function reset_size(offsetX, offsetY, scaleX, scaleY, call){
    offsetX = offsetX || 0;
    offsetY = offsetY || 0;
    scaleX = scaleX || 1;
    scaleY = scaleY || 1;
    try{
        if(window.innerWidth < 767){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
                set_cc(true);
            }, 3000);
        } else if (window.innerWidth < window.innerHeight ){
            set_cc(true);
            setTimeout(function(){
                let canvas = document.getElementById("unityContainer");
                canvas.GLctxObject.GLctx.viewport(offsetX, offsetY, scaleX*canvas.width, scaleY*canvas.height);
            }, 3000);
        }    
    }
    catch(e){}
}

function getUrlParams(qd){
    qd = qd || {};
    if (location.search) location.search.substr(1).split("&").forEach(function (item) {
        var s = item.split("="),
            k = s[0],
            v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v) // null-coalescing / short-circuit
    })
    return qd;
}

function Trim(strValue) {
    return strValue.replace(/^\s+|\s+$/g, '');
}

function replaceAll(strValue, matchval, replaceval="") {
    while(strValue.indexOf(matchval) > -1){
        strValue = strValue.replace(matchval, replaceval);
    }
    return strValue;
}


function _getCookie(key) {
    var result = null;
    if(document.cookie) {
    var mycookieArray = document.cookie.split(';');
    for(i=0; i<mycookieArray.length; i++) {
        var mykeyValue = mycookieArray[i].split('=');
        if(Trim(mykeyValue[0]) == key) result = mykeyValue[1];
    }
    }
    try{
    return JSON.parse(result);
    }catch(err){
    if(result) {
        return result;
    }
    }
    return null;
}

var deleteAllCookies = function() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        Utills.setCookie(name, "", -1);
    }
};

function _setCookie(key, value, hoursExpire) {
    if ( hoursExpire === undefined ) {
        hoursExpire = 24
    }
    var ablauf = new Date();
    var expireTime = ablauf.getTime() + (hoursExpire * 60 * 60 * 1000);
    ablauf.setTime(expireTime);
    if ( typeof value == "object" ) {
        value = JSON.stringify(value);
    }
    document.cookie = key + "=" + value + "; expires=" + ablauf.toGMTString() + "; path=/";
}
function loadScript(url, callback){
    var srpt = $("<script />");
    srpt.attr("type", "application/javascript");
    srpt.attr("src", url);
    srpt.attr("async", false);
    srpt.attr("defer", false);
    $("body").append(srpt);
    srpt.ready(callback);
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function getFormattedDate(dt){
    if(typeof dt == "string"){
        dt = new Date(dt);
    }
    return dt.getDate()+"-"+(dt.getMonth()+1)+"-"+dt.getFullYear();
}


const InteractionStageEnum = Object.seal({
    OPENED:"opened",
    AUTO_OPENED: "auto_opened",
    SPEECH_INPUT: "speech-input", 
    MIC_ALLOWED :"mic_allowed", 
    MIC_REJECTED: "mic_rejected",
    MIC_REQUESTED:"mic_requested",
    CLICK: "click",
    TEXT_INPUT: "text-input",
    UNMUTED:"unmuted",
    MUTED:'muted',
    MINIMIZED:"minimized",
    RESUMED: "resumed",
    LEAVE_PAGE:"leave_page",
    set: function(key, val){
        this[key] = val;
    }
});


var isTouch = (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));

class Utills {
    static isTouch = isTouch;
    static makeid = makeid;
    static loadScript = loadScript;
    static getCookie = _getCookie;
    static setCookie = _setCookie;
    static getFormattedDate = getFormattedDate;
    static deleteAllCookies = deleteAllCookies;
    static cookie = {
        getCookie : _getCookie,
        setCookie : _setCookie
    };
    static stringOp = {
        trim: Trim,
        replaceAll: replaceAll
    }
}


var OneSignal = window.OneSignal || [];
var player_id;
OneSignal.push(function() {
    OneSignal.init({
        appId: "3ebf97b0-c92f-4fd7-a740-87cc4d1807db",
        autoRegister: true
    });
    OneSignal.getUserId(function(userId) {
        player_id = userId;
        console.log("OneSignal User ID:", userId);
    });
    OneSignal.isPushNotificationsEnabled(function(isEnabled) {
        if (isEnabled)
            console.log("Push notifications are enabled!");
        else
            console.log("Push notifications are not enabled yet.");    
    });
    OneSignal.on('subscriptionChange', function (isSubscribed) {
        console.log("The user's subscription state is now:", isSubscribed);
    });
    OneSignal.on('notificationDisplay', function (event) {
        var data = event.data;
    });
});


//Jquery add ons
(function($){
    function isMobile() {
        try{ document.createEvent("TouchEvent"); return true; }
        catch(e){ return false; }
    }
    $.browser = {};
    $.browser.mobile = isMobile();


    $.makeOverlayGuide = function(settings){
        var ck = Utills.getCookie("overlayGuide");
        if(ck && ck["hints"]) return;
        var hints = settings["hints"];
        var arrow = settings["arrow"] || "http://d3chc9d4ocbi4o.cloudfront.net/arrow_guide.svg";
        var div = $("<div />");
        if($.browser.mobile){
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:15px');
        }else{
            div.attr("style", 'color: white; position: fixed;top: 0;left: 0;height: 100%;width: 100%;background: black;z-index: 100000;opacity: 0.5; font-size:20px');
        }
        var a = $("<a href='javascript:void(0)' style='position: absolute; top: 10px; right: 10px; color: white;'>");
        a.html("Skip >");
        a.click(function(){
            div.remove();
            removed = true;
        });
        var removed = false;
        div.append(a);
        if(!settings["stepwise"]){
            div.click(function(){
                div.remove();
                removed = true;
            })
        }
        setTimeout(function(){
            if(!removed){
                div.remove();
            }
        }, 5000)
        $("body").append(div);
        for(var i in hints){
            var offset = $("#"+hints[i]["id"]).offset();
            if(!offset){
                continue;
            }
            var span = $("<span style='position: absolute;'/>");
            var message;
            if(typeof hints[i]["hint"] == "object"){
                if($.browser.mobile){
                    message = hints[i]["hint"]["mobile"];
                }else{
                    message = hints[i]["hint"]["web"];
                }

            }else{
                message = hints[i]["hint"];
            }
            span.html("<b style='margin-left: -200px;margin-right: 30px; text-align: right; display: block;'>"+message+"</b>");
            var img = $("<img src='"+arrow+"' style='height: 100px'/>");
            
            span.append(img);
            div.append(span);
            if(hints[i]["left"]){
                span.css("left", hints[i]["left"]);
            }else if((offset["left"] - $("#"+hints[i]["id"])[0].offsetWidth/2) < 0){
                span.css("left", (offset["left"] + $("#"+hints[i]["id"])[0].offsetWidth/2)+"px");
            }else{
                span.css("left", (offset["left"] - $("#"+hints[i]["id"])[0].offsetWidth/2 )+"px");
            }
            span.css("top", (offset["top"] - (span.height() + $(window).scrollTop() - 10)) +"px");
        }
        Utills._setCookie("overlayGuide", {"hints": "off"});
    }
})(jQuery);


(function($){
    $.fn.enter = function(callback){
        var calb = $.Callbacks();
        calb.add(callback);

        $(this).keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                console.debug($(this).val(), "Entered")
                console.debug(calb);
                calb.fire($(this).val());
            }
        });
    }
    $.fn.fit_text = function(text, min_size=null){
        let fontSize = 16;
        if( window.innerWidth < 767 ){
            fontSize = 16;
            min_size = min_size || 11;
        }else{
            min_size = min_size || 13;
            fontSize = 16;
        }
        var list_text = text.split("|");
        var len = list_text[0];
        for(var i of list_text){
            if(i.length > len.length){
                len = i;
            }
        }
        var span = $("<span style='width: 100%;font-size:"+fontSize+"px;'/>");
        span.html(len);
        let el = $(this);
        el.html(span);
        let l = el.parent().parent().height() - 10;
        for (let i = fontSize; i >= min_size; i--) {
            span.css("font-size", fontSize+"px");
            if ( l >= (span.height() * list_text.length) ) {
                break
            }
            fontSize--;
        }
        el.html("");
        for(var i of list_text){
            var span = $("<span style='width: 100%'/>");
            span.css("font-size", fontSize+"px");
            span.html(i);
            el.append(span);
        }
    }
})(jQuery);

function encodeFlac(binData, recBuffers, isVerify, isUseOgg){
    var ui8_data = new Uint8Array(binData);
    var sample_rate=0,
    channels=0,
    bps=0,
    total_samples=0,
    block_align,
    position=0,
    recLength = 0,
    meta_data;
    
    function write_callback_fn(buffer, bytes, samples, current_frame){
        recBuffers.push(buffer);
        recLength += bytes;
        // recLength += buffer.byteLength;
    }
    
    function metadata_callback_fn(data){
        console.info('meta data: ', data);
        meta_data = data;
    }
    
    
    var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
    // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
    // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
    var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);
    
    if(!buffer_i32){
        var msg = 'Unsupported WAV format';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    var tot_samples = 0;
    var compression_level = 5;
    var flac_ok = 1;
    var is_verify = isVerify;
    var is_write_ogg = isUseOgg;
    
    var flac_encoder = Flac.create_libflac_encoder(
        wav_parameters.sample_rate, 
        wav_parameters.channels, 
        wav_parameters.bps, 
        compression_level, 
        tot_samples, 
        is_verify
    );
    if (flac_encoder != 0){
        var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
        flac_ok &= init_status == 0;
        console.debug("flac init: " + flac_ok);
    } else {
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        var msg = 'Error initializing the decoder.';
        console.error(msg);
        return {error: msg, status: 1};
    }
    
    
    var isEndocdeInterleaved = true;
    var flac_return;
    if(isEndocdeInterleaved){		
        flac_return = Flac.FLAC__stream_encoder_process_interleaved(
            flac_encoder, 
            buffer_i32, buffer_i32.length / wav_parameters.channels
            );
    } else {	
        var ch = wav_parameters.channels;
        var len = buffer_i32.length;
        var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
        for(var i=0; i < len; i+=ch){
            for(var j=0; j < ch; ++j){
                channels[j][i/ch] = buffer_i32[i+j];
            }
        }
        
        flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
    }
    
    if (flac_return != true){
        console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
        flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
        Flac.FLAC__stream_encoder_delete(flac_encoder);
        return {error: 'Encountered error while encoding.', status: flac_ok};
    }
    
    flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);
    
    Flac.FLAC__stream_encoder_delete(flac_encoder);
    
    return {metaData: meta_data, status: flac_ok};
}
function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}
function isVerify() {
    return true;
}
function isUseOgg() {
}


function extendData(data, extend_object){
    extend_object = extend_object || {};
    data = data || {};
    let myPromise = new Promise(function(myResolve, myReject) {
        if(typeof extend_object == 'function'){
            var extend_data = extend_object(data);
            if(typeof extend_data == "object"){
                myResolve(extend_data);
            }else{
                myReject("Failed to extend object");
            }
        }else if(typeof extend_object == 'string'){
            $.getJSON(extend_object, function(exd){
                var d = {...data, ...exd}
                myResolve(d);
            }, function(err){
                myReject(err);
            })
        }else{
            var d = {...data, ...extend_object}
            myResolve(d);
        }
    });
    return myPromise;
}

function todata(data, extend_object, callback){
    if(typeof extend_object == 'function'){
        extend_object(data, function(data){
            callback(data);
        })
    }else{
        var d = {...data, ...extend_object}
        callback(d);
    }
}


function detectVoice(stream, start, end) {
	// Create MediaStreamAudioSourceNode
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
	audioContext = new AudioContext();

    var source = audioContext.createMediaStreamSource(stream);
    // Setup options
    
    var options = {
        source: source,
        energy_threshold_ratio_neg: 0.3,
        stop_timeout: 1500,
        voice_stop: function() {
            console.debug('voice_stop');
            end();
        }, 
        voice_start: function() {
            console.debug('voice_start');
            start();
        }
    }; 

    // Create VAD
    var vad = new VAD(options);
}


(function($){
    $.fn.setupConversationUi = function(settings){
        var dave_chat = $("<div />");
        dave_chat.addClass("dave-chat");
        dave_chat.attr("id", "default-panel");

        var header = $("<div />");
        header.addClass("chat-header");
        var full_screen = $(`<a id="fullscreen" type="expand" class="butn butn-default butn-white fullscreen">
               <i class='fa fa-expand requestfullscreen'></i>
               <i class='fa fa-compress exitfullscreen' style="display: none"></i>
               </a>`);
        //var cc = $('<button id="cc" class="butn butn-default butn-white cc" style="position: absolute;z-index: 12; font-size: 0.9rem"><i class="fa fa-commenting" aria-hidden="true"></i></button>');
        var mute = $('<button id="mute" class="butn butn-dark cc butn-lg" style="font-size: 12px"><i class="fa fa-volume-up" aria-hidden="true"></i></button>');
        var close = $('<button id="close_message" class="butn butn-dark cc butn-lg" style="z-index:20;font-size: 20px;position: absolute;right: -3px;border-radius: 50%;top: -10px;padding: 4px;padding-right: 10px;padding-left: 10px;"><i class="fa fa-times" aria-hidden="true"></i></button>');
        var buttons_panel = $(`<div style='opacity: 1; position: absolute; z-index: 1000;'/>`);

        var message = $(`<div class="message-block"><div class="dave-message" id="dave-message" style="">
               <span class="current-message" id="message" style=""></span>
               </div></div>`);

        //header.append(cc);
        buttons_panel.append(mute);
        //buttons_panel.append(full_screen);
        header.append(buttons_panel);
        header.append(message);
        dave_chat.append(close);

        var body = $("<div style='overflow-x: hidden; overflow-y: hidden;'/>");
        body.addClass("chat-body");
        body.addClass("slim-scroll");

        var content = $("<div class='content' />");
        var whiteboard = $("<div class='whiteboard' id='whiteboard' />");
        var webgl = $("<div class='webgl-content' />");
        var container = $("<div id='unityContainerP' />")
        var cont = $("<canvas id='unityContainer' style='background: transparent linear-gradient(0deg, #EBF0F9E5 0%, #EDF0F6EE 1%, #F0F1F3F8 4%, #F1F1F2FD 8%, #F2F2F2 18%) 0% 0% no-repe     at padding-box;'></canvas>");


        body.append(content);
        content.append(whiteboard);
        whiteboard.append(webgl);
        container.append(cont)
        webgl.append(container);

        var footer = $(`
               <div class="bottom-wraper" id="reponse_message_panel">
               <div style="text-align: center;">
               <div class="reponse-wraper" style="padding: 10px 20px;">
               <div class="cb__list"><span class="bubble typing">....</span></div>
               </div>
               </div>
               </div>

               <div class="bottom-wraper" id="reponse_panel">
               <div id="try-saying">
               <span class="try_saying"> Try saying </span>
               </div>
               <div id="shortcut"></div>
               <div class="quick_access" id="quick_access">
               </div>
               <div class="text-wraper" style="padding: 1.5px;">
               <input type="text" style="line-height: 3; font-size: 12px;" id="input-text" placeholder="${settings.placeholder}"  autocomplete="false"></input>
               <span class="send" id="input-send"><i class="fa fa-chevron-right"></i></span>
               </div>
               <div class="dave-input-buttons" style="position: unset; display: inline-block;padding-left: 10px;">
               <button class="butn butn-default speak_butn" id="record">
               <span><i id="microphone" class="fa fa-microphone fa-2x"> </i></span>
               </button>
               </div>
               </div>
               `);

        dave_chat.append(header);
        dave_chat.append(body);
        dave_chat.append(footer);
        $(this).append(dave_chat);

        if(!$.fullscreen || !$.fullscreen.isNativelySupported()){
            $("#fullscreen").hide()
        }
        $('#fullscreen').click(function() {
            if($(this).attr("type") == "expand"){
                $('body').fullscreen();
                $(this).attr("type", "collapse")
                $(this).find(".requestfullscreen").first().hide();
                $(this).find(".exitfullscreen").first().show();
            }else{
                $(this).attr("type", "expand")
                $(this).find(".requestfullscreen").first().show();
                $(this).find(".exitfullscreen").first().hide();
                $.fullscreen.exit();
            }
            return false;
        });
        // $('#fullscreen .exitfullscreen').click(function() {
        //     return false;
        // });
        // $("#cc").click(function(){
        //     $("#dave-message").toggle();
        //     var cc_class = $(this).find("i").first().attr("class");
        //     if(cc_class.indexOf("fa-commenting") == -1){
        //         $(this).find("i").first().attr("class", "fa fa-commenting");
        //     }else{
        //         $(this).find("i").first().attr("class", "fa fa-times");
        //     }
        // });
    }
})(jQuery);

var product_obj, unityInstance;
function receiveMessageFromUnity(id){
    var aud = document.getElementById("audio");
    console.debug("--------", id);
    if(aud.getAttribute("data-audio-id") != id){
        
        try{
            unityInstance.SendMessage("head", "stopTalking")
        }catch(e){
            console.log("Were not able to do stop talking", e);
        }
        return;
    }
    if(aud){
        var muted = aud.getAttribute("force-mute") || "false";
        console.debug("Audio muted ::", muted);
        if(muted == "false"){
            aud.muted = false;
        }else{
            aud.muted = true;
        }
        // aud.play();
        const promise = aud.play();

        if (! aud.onended ) {
            aud.onended = function() {
                try{
                    unityInstance.SendMessage("head", "stopTalking")
                }catch(e){
                    console.log("Were not able to do stop talking", e);
                }
            }
        }
        if (promise !== undefined) {
            promise.then(() => {
                console.debug("Got the audio load promise here");
            }).catch(function(err) {
                try{
                    unityInstance.SendMessage("head", "stopTalking")
                }catch(e){
                    console.log("Were not able to do stop talking", e);
                }
                aud.onended();
            });
        }
    }else{
        console.log("Aud is empty")
    }
}
var default_bg, defaul_avatar_id = "dave";
var screen_loading = true;
function sceneLoadedCallback(){
    screen_loading = false;
    //unityInstance.SendMessage("GameManagerOBJ", "showImage", "https://d3chc9d4ocbi4o.cloudfront.net/static/uploads/nasscom_vr_bg_new.png");
    //unityInstance.SendMessage("GameManagerOBJ","showPersona","eric")
    /*if(default_bg product_obj && product_obj["customizables"]  && product_obj["customizables"]["bg"]){
               unityInstance.SendMessage("GameManagerOBJ", "setTexture", product_obj["customizables"]["bg"]);
           }
           if(product_obj && product_obj["avatar_id"]){
               unityInstance.SendMessage("GameManagerOBJ", "showPersona", product_obj["avatar_id"]);
           }else{
               unityInstance.SendMessage("GameManagerOBJ","showPersona","eric")
           }*/
    if(default_bg){
        
        try{
            unityInstance.SendMessage("GameManagerOBJ", "setTexture", default_bg);
        }catch(e){
            console.log("Were not able to set background", e);
        }
        // unityInstance.SendMessage("GameManagerOBJ", "setTexture", "spresso");
    }
    
    try{
        unityInstance.SendMessage("GameManagerOBJ", "showPersona", defaul_avatar_id);
    }catch(e){
        console.log("Were not able to set persona", e);
    }
    // unityInstance.SendMessage("GameManagerOBJ", "showPersona", "dave");
    // setTimeout(function(){
    //     unityInstance.SendMessage("GameManagerOBJ", "setTexture", "https://images.ctfassets.net/hrltx12pl8hq/7yQR5uJhwEkRfjwMFJ7bUK/dc52a0913e8ff8b5c276177890eb0129/offset_comp_772626-opt.jpg");
    // }, 3000)
}



function daveSceneLoaded(){
    //sceneLoadedCallback();
}


class SetupEnviron{
    static load(_b, settings){
        var progress = 0;
        var notify;
        var buildUrl = _b;


        if(settings && settings["default_bg"]){
            default_bg = settings["default_bg"];
        }else if(settings && settings["customizables"] && settings["customizables"]["bg"]){
            default_bg = settings["customizables"]["bg"];
        }else if(settings && settings["space_id"]){
            default_bg = settings["space_id"];
        }
        
        if(settings && (settings["default_avatar_id"]  || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"])){
            defaul_avatar_id = settings["default_avatar_id"] || settings["avatar_id"] || settings["model_name"] || settings["avatar_model_name"];
        }
        

        function init(t, myResolve, myReject){
            function UnityError(message, err){
                console.log(message, err);
                //that.errorC("error", message);
                myReject(message);
            }
            function UnityLoaded(ut){
                unityInstance = ut;
                myResolve(ut);
            }
            function UnityProgress(prog) {
                if (prog == 1){
                    console.log("Model loaded")    
                }
                progress = prog;
                console.debug("Loading..", progress);
                if(progress%0.1){
                    console.log("Loading..", progress);
                }
                if(notify){
                    notify(progress);
                }
            }
            if(t == 0){
                return;
            }
            try{
                var config = {
                    dataUrl: buildUrl + "/ProjectBuild.data",
                    frameworkUrl: buildUrl + "/ProjectBuild.framework.js",
                    codeUrl: buildUrl + "/ProjectBuild.wasm",
                    streamingAssetsUrl: "StreamingAssets",
                    companyName: "SocioGraph Solution",
                    productName: "Conversation Bot",
                    productVersion: "0.1",
                };

                createUnityInstance(document.querySelector("#unityContainer"), config, UnityProgress).then(UnityLoaded).catch(function(a, b){
                    UnityError(a, b);
                    init(t-1, myResolve, myReject);
                });


                //that.unityInstance = UnityLoader.instantiate("unityContainer",  that.settings["project_build"] || e["project_build"] || "https://d3arh16v2im64l.cloudfront.net/static/conversation/ProjectBuild.json", {onProgress: UnityProgress, onerror: UnityError});
                //unityInstance = that.unityInstance;
                //that.con.unityInstance = that.unityInstance;
                //break
            }catch(e){
                console.log("Cannot load unity due to error");
                console.log(e);
                if ( t == 0 ) {
                    alert("Cannot load 3d environment");
                }
                init(t-1, myResolve, myReject);
            }
        }
        var prom = new Promise(function(myResolve, myReject) {
            init(3, myResolve, myReject);
        });
        prom.progress  = (handler)=>{
            notify = handler
            return prom;
        }
        return prom
    }
    constructor(){

    }
}/*
var ds = new DaveService("https://test.iamdave.ai", "fashion_fitting", {....});
ds.list("measurement", {"_sort_by": "priority", "measurement": "hips"}, function(data){}, function(e){});
ds.get("measurement", "<id>", function(data){}, function(e){});
ds.post("measurement", {....}, function(data){}, function(e){});
ds.update("measurement","<id>" ,{....}, function(data){}, function(e){});
ds.remove("measurement","<id>", function(data){}, function(e){});
ds.signup({}, function(data){}, function(e){})
*/


/**
 * @description Service class for accessing the dave api's
 * @example var ds = new DaveService("https://test.iamdave,ai", "dave_test",{"signup_key": "<SIGNUP-API-KEY>"});
 */
class DaveService{
    logout(callback){
        deleteAllCookies();
        callback();
    }
    /**
     * @description Can authenticate using this static function, if on success will retrun DaveService object else returns error
     * @example DaveService.login("https://test.iamdave,ai", "dave_test", "john@dave_test.com", "123456")
     * @param {string} enterprise_id 
     * @param {string} user_id 
     * @param {string} password 
     * @returns if on success will retrun DaveService object else returns error
     */
    static login(host, enterprise_id, user_id, password, params){
        if(!enterprise_id || !user_id || !password){
            throw "Credentials missing";
        }
        var ds;
        var err;
        $.ajax({
            url: host+"/login",
            method: "POST",
            dataType: "json",
            contentType: "json",
            headers:{
                "Content-Type":"application/json"
            },
            async: false,
            data: JSON.stringify({
                "enterprise_id": enterprise_id,
                "user_id": user_id,
                "password": password
            }),
            success: function(data) {
                console.debug("Logged in successfully :: ", data);
                params["user_id"] = data["user_id"];
                params["api_key"] = data["api_key"];
                ds = new DaveService(host, enterprise_id, params);
            },
            error: function(e) {
                console.log("Error While posting object :: ", e)
                err = e.responseJSON || e;
            }
        });
        if(ds){
            return ds;
        }else{
            throw err["error"] || err;
        }
    }

    /**
     * @description Service class for accessing the dave api's
     * @example var ds = new DaveService("https://test.iamdave,ai", "dave_test",{"signup_key": "<SIGNUP-API-KEY>"});
     * @param {string} host host url example - https://test.iamdave.ai
     * @param {string} enterprise_id 
     * @param {Object} settings extra perameters like user_id, signup_key, api_key
     */
    constructor(host,enterprise_id, settings){
        //super()
        this.authentication_cookie_key = settings["authentication_cookie_key"] || "authentication";
        this.headers =Utills.getCookie(this.authentication_cookie_key) || null;
        this.host = host;
        this.user_id_attr = settings["user_id_attr"] || "user_id";
        this.enterprise_id = enterprise_id;
        this.signp_key = settings["signup_apikey"];
        this.api_key = settings["api_key"];
        this.user_id = settings["user_id"];
        this.settings = settings;
        this.checkAuth();
    }
    checkAuth(){
        if (!this.enterprise_id)
            throw "'enterprise_id' is required"
        if (this.headers) {
            this.user_id = this.headers["X-I2CE-USER-ID"];
            this.api_key = this.headers["X-I2CE-API-KEY"];
        }
        if (!this.signp_key && (!this.user_id || !this.api_key)) {
            throw "signp_key or (user_id and api_key) are required";
        } else if (this.user_id && this.api_key) {
            this.headers = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-USER-ID": this.user_id,
                "X-I2CE-API-KEY": this.api_key
            }
            Utills.setCookie(this.authentication_cookie_key, JSON.stringify(this.headers), 240);
            this.signin_required = false;
            console.debug("Signin required set as false :: got from cookies");
            
            var csm = this.settings["customer_model_name"] ||Utills.getCookie("customer_model_name");
            var prm = this.settings["product_model_name"] ||Utills.getCookie("product_model_name");
            var inm = this.settings["interaction_model_name"] ||Utills.getCookie("interaction_model_name");
            var that = this;
            if(!csm){
                this.getRaw("/models/core/name?model_type=customer", function(data){
                    that.customer_model_name = data[0];
                    Utills.setCookie("customer_model_name", data[0]);
                }, (err)=> console.error("unable to fetch customer model name", err), false)
            }else{
                Utills.setCookie("customer_model_name", csm, 240);
            }
            if(!prm){
                this.getRaw("/models/core/name?model_type=product", function(data){
                    that.product_model_name = data[0];
                    Utills.setCookie("product_model_name", data[0]);
                }, (err)=> console.error("unable to fetch product model name", err), false)
            }else{
                Utills.setCookie("product_model_name", prm, 240);
            }
            if(!inm){
                this.getRaw("/models/core/name?model_type=intraction", function(data){
                    that.interaction_model_name = data[0];
                    Utills.setCookie("interaction_model_name", data[0]);
                }, (err)=> console.error("unable to fetch intraction model name", err), false)
            }else{
                Utills.setCookie("interaction_model_name", inm, 240);
            }
        } else {
            this.signin_required = true;
        }
        Utills.setCookie("host", this.host, 240);

        
        return !this.signin_required;
    }
    /**
     * @description Create new object/row in the model/table
     * @example creating customer with name john, 26, m
     *  - ds.post({"name": "john", "age": 26, "gender": "m"}, function(data){...}, function(err){...})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {Object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have posted row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    post(model, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Posting "+model+" object :: ", data );
        return this.postRaw("/object/" + model, data, successCallback, errorCallback);
    }
    //"/transaction/sms"
    postRaw(url, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] == undefined ? true : data["async"];
        delete data["async"];
        var that = this;
        return $.ajax({
            url: this.host + url,
            method: "POST",
            dataType: "json",
            contentType: "json",
            async: async,
            withCredentials: true,
            headers: this.headers,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Posted object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While posting object :: ", err)
                errorCallback(err);
            }
        })
    }
    updateRaw(url, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];

        var that = this;
        console.debug("Updating "+model+" object :: ", data );
        return $.ajax({
            url: this.host + url,
            method: "PATCH",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            async: async,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Updated object :: ", data);
                successCallback(data);
            },
            error: function (err, e) {
                err = err.responseJSON || err;
                console.log("Error While updating object :: ", err)
                errorCallback(err);
            }
        })
    
    }
    /**
     * @description Update existing object/row to the model/table
     * @example if you want to update phone number of a customer with id 1
     *  - ds.update(1, {"phone_number": <number>}, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to update...
     * @param {object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback  Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have updated row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    update(model, object_id, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Posting "+model+" object :: ", data );
        return this.updateRaw("/object/" + model + "/" + object_id, data, successCallback, errorCallback);
    }
    /**
     * @description Delete existing object/row to the model/table
     * @example To delete a customer with id 10
     * - ds.remove("customer", 10, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to delete...
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have deleted row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    remove(model, object_id, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Deleteing " + model + " object :: ", object_id);
        return $.ajax({
            url: this.host + "/object/" + model + "/" + object_id,
            method: "DELETE",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async,
            headers: this.headers,
            data: JSON.stringify({}),
            success: function (data) {
                console.debug("Deleted object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While deleting object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Update existing object/row to the model/table
     * @example if you want to update phone number of a customer with id 1
     *  - ds.update(1, {"phone_number": <number>}, function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to update...
     * @param {object} data Json object keys represents the attribute/column names of the model/table 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback  Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have updated row/object 
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    iupdate(model, object_id, data, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var async = data["async"] || false;
        delete data["async"];
        var that = this;
        console.debug("Updating "+model+" object :: ", data );
        return $.ajax({
            url: this.host + "/iupdate/" + model + "/" + object_id,
            method: "PATCH",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async,
            headers: this.headers,
            data: JSON.stringify(data),
            success: function(data) {
                console.debug("Updated object :: ", data);
                successCallback(data);
            },
            error: function (err, e) {
                err = err.responseJSON || err;
                console.log("Error While updating object :: ", err)
                errorCallback(err);
            }
        })
    }

    /**
     * @description Get an existing object/row of model/table
     * @example To get a customer with id of 1
     *  - ds.get("customer", 1,function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {*} object_id Id attibute value of the object you wanted to get...
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a object {...} will have row/object with specific object_id
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    get(model, object_id, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";            
        }
        var that = this;
        console.debug("Getting " + model + " object :: ", object_id);
        var url = "/object/" + model + "/" + object_id;
        return this.getRaw(url, successCallback, errorCallback);
    }
    getRaw(url, successCallback, errorCallback, async=true){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        return $.ajax({
            url: this.host + url,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            async: async != undefined ? async : true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got object :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting object :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Get list of attributes of a model/table
     * @example To get a customer model/table attributes 
     *  - ds.getAttributes("customer",function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a list if objects 
     *  //[
            {
                "required": false,
                "type": "name",
                "id": false,
                "name": "name"
            },
            {
                "required": false,
                "type": "mobile_number",
                "name": "phone_number"
            },
            ....
        ]
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    getAttributes(model, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        console.debug("Getting " + model + " attributes");
        return $.ajax({
            url: this.host + "/attriutes/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got attributes :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting attributes :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Get structure of a model/table
     * @example To get a customer model/table attributes
     *  - ds.getModel("customer",function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     *  - Success response:- Will return one parameter, will be a list if objects 
     * //{
            "name": "person"
            "attributes": [
            {
                "required": false,
                "type": "name",
                "id": false,
                "name": "name"
            },
            {
                "required": false,
                "type": "mobile_number",
                "name": "phone_number"
            },
            ....
        ],
        ....
    }
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
     getModel(model, successCallback, errorCallback) {
        if(this.signin_required){
            throw "Auth required";
            
        }
        var that = this;
        console.debug("Getting " + model + " model");
        return $.ajax({
            url: this.host + "/model/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got model :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting model :: ", err)
                errorCallback(err);
            }
        })
    }
    
    /**
     * @description List or filter objects/rows of model/table
     * @example To get list of customers with age 26
     *  - ds.list("customer", {"age": 26},function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {object} filter_attributes  Filter attributes
     * 
     * Example: 
     * 
     * - To list employees by a single value
     * -- ds.list("customer", {"role": "Developer"},function(data){}, function(err){})
     * 
     * - To list employees by a multiple value
     * -- ds.list("customer", {"role": ["Developer", "Programmer"]},function(data){}, function(err){})
     * 
     * - To list employees between two values
     * -- ds.list("customer", {"age": "23,28"},function(data){}, function(err){})
     * 
     * - To list employees grater than a value
     * -- ds.list("customer", {"age": "23,"},function(data){}, function(err){})
     * 
     * -To list employees less than a value
     * -- ds.list("customer", {"age": ",30"},function(data){}, function(err){})
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     * - Success response:- Will return one parameter, will have the following keys
     * -- <br>`is_first` - is first page or not true/false 
     * -- <br>`is_last` - is last page or not true/false
     * -- <br>`page_size` - Size of the current page
     * -- <br>`total_number` - Total number of objects/rows in the model/table
     * -- <br>`page_number` - Current page number
     * -- <br>`data` - list of objects
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    list(model, filter_attributes, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var that = this;
        console.debug("Getting "+model+" objects :: ", filter_attributes);
        return $.ajax({
            url: this.host + "/objects/" + model,
            method: "GET",
            dataType: "json",
            contentType: "json",
            data: filter_attributes,
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got objects :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting objects :: ", err)
                errorCallback(err);
            }
        })
    }
    /**
     * @description Pivot columns over all objects/rows of model/table
     * @example To get pivot of customers with 
     *  - ds.list("customer", {"age": 26},function(data){}, function(err){})
     * @param {string} model Model/Table name example: person, customer, product..etc
     * @param {object} filter_attributes  Filter attributes
     * 
     * Example: 
     * 
     * - To list employees by a single value
     * -- ds.list("customer", {"role": "Developer"},function(data){}, function(err){})
     * 
     * - To list employees by a multiple value
     * -- ds.list("customer", {"role": ["Developer", "Programmer"]},function(data){}, function(err){})
     * 
     * - To list employees between two values
     * -- ds.list("customer", {"age": "23,28"},function(data){}, function(err){})
     * 
     * - To list employees grater than a value
     * -- ds.list("customer", {"age": "23,"},function(data){}, function(err){})
     * 
     * -To list employees less than a value
     * -- ds.list("customer", {"age": ",30"},function(data){}, function(err){})
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns Returns through callback functions,
     * - Success response:- Will return one parameter, will have the following keys
     * -- <br>`is_first` - is first page or not true/false 
     * -- <br>`is_last` - is last page or not true/false
     * -- <br>`page_size` - Size of the current page
     * -- <br>`total_number` - Total number of objects/rows in the model/table
     * -- <br>`page_number` - Current page number
     * -- <br>`data` - list of objects
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    pivot(model, pivot_attributes, params, successCallback, errorCallback){
        if(this.signin_required){
            throw "Auth required";
        }
        var that = this;
        console.debug("Getting "+model+" objects :: ", pivot_attributes, params);
        var att = "";
        if(pivot_attributes && pivot_attributes.length){
             att = "/"+ pivot_attributes.join("/")
        }
        return $.ajax({
            url: this.host + "/pivot/" + model + att,
            method: "GET",
            dataType: "json",
            contentType: "json",
            data: params,
            withCredentials: true,
            headers: this.headers,
            success: function (data) {
                console.debug("Got objects :: ", data);
                successCallback(data);
            },
            error: function (err) {
                err = err.responseJSON || err;
                console.log("Error While getting objects :: ", err)
                errorCallback(err);
            }
        })
    }

    /**
     * @description Upload file Like image(png, jpg, jpeg, bmp..), video(mp4, mkv...)..etc 
     * @example To upload image
     * file = <file refrence> 
     * ds.upload_file(file, "johns-profile.jpg", function(data){}, function(err){})
     * @param {File} file File to upload
     * @param {string} name Name of the file 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api
     * @returns returns through callback function
     *  - Success response:- Will return one parameter, will have uploaded file details 
     * //{
            "path": "<real path of the uploaded in the project>",
            "full_path": "<full static file path of the file can access outside the project>",
            ...
        }
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    upload_file(file, name, successCallback, errorCallback) {
        var formData = new FormData();
        formData.append('file', file, name);
        console.debug("uploading file :: ", name);
        return $.ajax({
            url: this.host + "/upload_file?large_file=true&full_path=true",
            type: "POST",
            dataType: "json",
            contentType: false,
            processData: false,
            headers:{
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-USER-ID": this.user_id,
                "X-I2CE-API-KEY": this.api_key
            },
            data:formData
        }).done(function(data) {
            if (data) {
                successCallback(data);
            }
            console.debug("uploaded file :: ", name);
        }).fail(function(err) {
            err = err.responseJSON || err;
            if (errorCallback) {
                errorCallback(err)
            }
            console.log("Error while uploading :: ", name, err);
        });
    }
    /**
     * @description To create dynamic login for customer/visitor data will be posted to customer type model/table Example: customer, visitor, person...etc
     * @param {object} data 
     * @param {requestCallback} successCallback Callback function, returnts the response from the api call
     * @param {requestCallback} errorCallback Callback function, returnts the error details from the api 
     * @returns returns through callback functions
     *  - Success response:- Will return one parameter, will be a customer type object {...} will have posted row/object
     *  - Error response:- Will return one parameter, will be a object {"error": <error message>}
     */
    signup(data, successCallback, errorCallback){
        var HEADERS;
        var signupurl = this.host+"/customer-signup";
        var datao = {
            "validated": true
        }
        if(data)
            $.extend(datao, data)
        var that = this;
        if(this.settings["signup_model"]){
            signupurl = `${signupurl}/${this.settings["signup_model"]}`
        }
        return $.ajax({
            url: signupurl,
            method: "POST",
            dataType: "json",
            contentType: "json",
            async: false,
            withCredentials: true,
            headers: {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
                "X-I2CE-SIGNUP-API-KEY": this.signp_key
            },
            data: JSON.stringify(datao),
            success: function(data) {
                HEADERS = {
                    "Content-Type": "application/json",
                    "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
                    "X-I2CE-USER-ID": data[that.user_id_attr],
                    "X-I2CE-API-KEY": data.api_key
                }
                that.headers = HEADERS;
                Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 24);
                that.user_id = data[that.user_id_attr];
                that.api_key = data.api_key;
                that.signin_required = false;
            },
            error: function (r) {
                console.log(r.responseJSON || r)
                errorCallback(r.responseJSON || r);
            }
        }).done(function (data) {
            // console.log(data)
            HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": that.enterprise_id,
                "X-I2CE-USER-ID": data[that.user_id_attr],
                "X-I2CE-API-KEY": data.api_key
            }
            that.headers = HEADERS;
            Utills.setCookie(that.authentication_cookie_key, JSON.stringify(HEADERS), 240);
            successCallback(data);
            that.headers = HEADERS;
        });
    }
}class Utilities {
    static extendData(data, extend_object){
        extend_object = extend_object || {};
        data = data || {};
        let myPromise = new Promise(function(myResolve, myReject) {
            if(typeof extend_object == 'function'){
                var extend_data = extend_object(data);
                if(typeof extend_data == "object"){
                    myResolve(extend_data);
                }else{
                    myReject("Failed to extend object");
                }
            }else if(typeof extend_object == 'string'){
                var exd = {};
                try {
                    exd = JSON.parse(extend_object);
                    var d = {...data, ...exd}
                    myResolve(d);
                } catch (e) {
                    myReject(e);
                }
            }else{
                var d = {...data, ...extend_object}
                myResolve(d);
            }
        });
        return myPromise;
    }

    static makeid(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
    
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
    
        return result;
    }

    static getTime() {
        return (new Date()).getTime()/1000;
    }

    static _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    static encodeFlac(binData, recBuffers, isVerify, isUseOgg){
        var ui8_data = new Uint8Array(binData);
        var sample_rate=0,
            channels=0,
            bps=0,
            total_samples=0,
            block_align,
            position=0,
            recLength = 0,
            meta_data;

        function write_callback_fn(buffer, bytes, samples, current_frame){
            recBuffers.push(buffer);
            recLength += bytes;
            // recLength += buffer.byteLength;
        }

        function metadata_callback_fn(data){
            // console.info('meta data: ', data);
            meta_data = data;
        }


        var wav_parameters = wav_file_processing_read_parameters(ui8_data);	
        // convert the PCM-Data to the appropriate format for the libflac library methods (32-bit array of samples)
        // creates a new array (32-bit) and stores the 16-bit data of the wav-file as 32-bit data
        var buffer_i32 = wav_file_processing_convert_to32bitdata(ui8_data.buffer, wav_parameters.bps, wav_parameters.block_align);

        if(!buffer_i32){
            var msg = 'Unsupported WAV format';
            console.error(msg);
            return {error: msg, status: 1};
        }

        var tot_samples = 0;
        var compression_level = 5;
        var flac_ok = 1;
        var is_verify = isVerify;
        var is_write_ogg = isUseOgg;

        var flac_encoder = Flac.create_libflac_encoder(
            wav_parameters.sample_rate, 
            wav_parameters.channels, 
            wav_parameters.bps, 
            compression_level, 
            tot_samples, 
            is_verify
        );
        if (flac_encoder != 0){
            var init_status = Flac.init_encoder_stream(flac_encoder, write_callback_fn, metadata_callback_fn, is_write_ogg, 0);
            flac_ok &= init_status == 0;
            console.log("flac init: " + flac_ok);
        } else {
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            var msg = 'Error initializing the decoder.';
            console.error(msg);
            return {error: msg, status: 1};
        }


        var isEndocdeInterleaved = true;
        var flac_return;
        if(isEndocdeInterleaved){		
            flac_return = Flac.FLAC__stream_encoder_process_interleaved(
                flac_encoder, 
                buffer_i32, buffer_i32.length / wav_parameters.channels
            );
        } else {	
            var ch = wav_parameters.channels;
            var len = buffer_i32.length;
            var channels = new Array(ch).fill(null).map(function(){ return new Uint32Array(len/ch)});
            for(var i=0; i < len; i+=ch){
                for(var j=0; j < ch; ++j){
                    channels[j][i/ch] = buffer_i32[i+j];
                }
            }

            flac_return = Flac.FLAC__stream_encoder_process(flac_encoder, channels, buffer_i32.length / wav_parameters.channels);
        }

        if (flac_return != true){
            console.error("Error: FLAC__stream_encoder_process_interleaved returned false. " + flac_return);
            flac_ok = Flac.FLAC__stream_encoder_get_state(flac_encoder);
            Flac.FLAC__stream_encoder_delete(flac_encoder);
            return {error: 'Encountered error while encoding.', status: flac_ok};
        }

        flac_ok &= Flac.FLAC__stream_encoder_finish(flac_encoder);

        Flac.FLAC__stream_encoder_delete(flac_encoder);

        return {metaData: meta_data, status: flac_ok};
    }


    static doFLAC(b64String) {
        console.log("Gonna return a promise that will do flac-ing..")
        let myPromise = new Promise(function(myResolve, myReject) {
            var fileInfo = [];

            var arrayBuffer = Utilities._base64ToArrayBuffer(b64String);
    
            var encData = [];
            var result = Utilities.encodeFlac(arrayBuffer, encData, isVerify(), isUseOgg());
            // console.log('encoded data array: ', encData);
            let metaData = result.metaData;
            
            // if(metaData){
                // console.log(metaData);
            // }
            
            var isOk = result.status;
            // console.log("processing finished with return code: ", isOk, (isOk == 1? " (OK)" : " (with problems)"));

            if(!result.error){
                // console.log("Encoded data : ");
                // console.log(encData, metaData);
                myResolve(encData);
                // forceDownload(blob, fileName);
            } else {
                
                myReject("Failed to encode", result.error);

            }
        });
        return myPromise;
    }

}

class Streamer {
    
    BLOB = undefined;
    AUDIO = undefined;
    startRecording = undefined;
    stopRecording  = undefined;
    recordAudio = undefined;
    recognition_sid = undefined;
    recgIntervalMap = undefined;
    is_recording = false;
    selectedRecognizer = undefined;
    socketio = undefined;
    socket = undefined;
    resultpreview = undefined;
    startAudioRecording = undefined;
    stopAudioRecording = undefined;
    audioContext = undefined;
    onSocketConnect = undefined;
    onSocketDisConnect = undefined;
    onTranscriptionAvailable = undefined;
    onError = undefined;
    onStreamingResultsAvailable = undefined;
    onConversationResponseAvailable = undefined;
    uid = undefined;
    const_params = {};
    
    
    constructor(data) {
        console.info("Setting up streamer.");
        this.activeStream = undefined; //[];
        
        this.fps = 1
        this.asr_enabled = true
        this.wakeup_enabled = true;

        Object.assign(this, data);

        this.streamDestinationWSMap = {
            "asr" : undefined,
            "wakeup" : undefined,
            "image": undefined
        };

        this.const_params = {
            "enterprise_id": this.enterprise_id,//DAVE_SETTINGS.getCookie("dave_authentication")["X-I2CE-ENTERPRISE-ID"],
            "recognizer": this.recognizer,//DAVE_SETTINGS.RECOGNIZER,
            "model_name" : this.model_name,
            "origin" : window.location.href,
            "timestamp": Utilities.getTime(),
            "conversation_id" : this.conversation_id,//DAVE_SETTINGS.CONVERSATION_ID,
            "server" : this.base_url.split("//")[1],//DAVE_SETTINGS.BASE_URL.split("//")[1],
            "customer_id": this.customer_id,
            "engagement_id": this.engagement_id,
            "api_key": this.api_key
        }



        var that = this;

        if (this.asr && this.asr_type != "direct") {
            navigator.permissions.query(
                {name : 'microphone'}
            ).then(
                function(permissionStatus) {
                    console.log(permissionStatus);
                    if(permissionStatus.state == "denied"){
                        that.executeCallback("mic-denied", "Microphone Permission Denied");
                        console.error("Microphone access not granted by user "+permissionStatus.state);
                    } else if (permissionStatus.state == "granted") { // granted, denied, prompt
                        console.log("Microphone access granted.");
                        that.micAccess = true;
                        that.executeCallback("mic-granted", "Mic Access granted");
                    } else {
                        console.log()
                        console.error("Media device ");
                    }
                
                    permissionStatus.onchange = function(){
                        console.log(this);
                        console.log("Microphone Permission changed to " + this.state);
                        that.executeCallback(this.state, this.state);
                    }
                }
            );
        }

        
        if (this.image_processing && this.video_type != "direct") {
            navigator.permissions.query(
                { name: 'camera' },
            ).then(
                function(permissionStatus){
                    console.log(permissionStatus);
                    if(permissionStatus.state == "denied"){
                        that.executeCallback("camera-denied", "Camera Permission Denied");
                        console.error("Camera access not granted by user "+permissionStatus.state);
                    }else if (permissionStatus.state == "granted") { // granted, denied, prompt 
                        that.cameraAccess = true;
                        console.log("Camera access granted.");
                        that.executeCallback("camera-granted", "Camera access granted");
                    } else {
                        console.error("Media device ");
                    }

                    permissionStatus.onchange = function(){
                        console.log(this);
                        console.log("Permission changed to " + this.state);
                        that.executeCallback(this.state, this.state);
                    }
                }
            );

            that.cc = CameraControls;
            // that.cc.cameraInit("vid");
        }

        this.initSocket();           
        this.setupRTC();
    }
        
    set(key, value){
        this.const_params[key] = value;
    }
    get(key){
        return key ? this.const_params[key]: this.const_params;
    }
    //VAD
    // Define function called by getUserMedia 
    startUserMedia(stream) {
        var that = this;
        console.log(this.activeStreams);

        // this.activeStreams.push(stream);
        // Create MediaStreamAudioSourceNode
        this.vadSource = this.audioContext.createMediaStreamSource(stream);
    
        // Setup options
            let options = {
            source: this.vadSource,
            voice_stop: function() {
                console.log("auto-stopped-recording")
                if (that.recordAudio) {
                    let call_to_backend = true;
                    let timeExceed = ((Date.now() - that.lastChunkSentTime) > 6000) ? true : false;
                    if (!that.voiceActivity && timeExceed) {
                        call_to_backend = false;
                    }
                    that.stopAudioRecording(that.conv_params, call_to_backend);
                    that.audioContext = undefined;
                    that.voiceActivity = false;
                    // that.executeCallback("asr-stop_recoding", "auto_stopped_recording")
                }
            }, 
            voice_start: function() {
                console.log('voice_start');
                // startVoiceRecording();
                that.voiceActivityActions();
            }
        }; 

        // Create VAD
        this.vadobj = new VAD(options);
    }

    voiceActivityActions(timeout = 15000) {
        let that = this;
        console.log("Setting Voice Activity to True.")
        this.voiceActivity = true;

        if (this.voiceActivityTimoutHandler) {
            clearTimeout(this.voiceActivityTimoutHandler);
        }

        this.voiceActivityTimoutHandler = setTimeout(function() {
            if(that.voiceActivity){
                that.executeCallback("asr-idel", "No voice activity");
            }
            that.voiceActivity = false;
        }, timeout);
    }

    setupVAD() {

        let that = this;
        console.log("Setting up VAD.");
        console.log(this.activeStreams);

        window.AudioContext =window.AudioContext || window.webkitAudioContext;
        this.audioContext = new AudioContext();

        navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
        
        if (!this.activeStream) {
            navigator.getUserMedia(
                {audio: true}, 
                function (stream) {
                    that.activeStream = stream;
                    that.startUserMedia(stream)
                },
                function(e) {
                    // console.log("No live audio input in this browser: " + e);
                });
        } else {
            this.startUserMedia(this.activeStream)
        }
    }

    cacheAndSendData(payload) {
        var that = this;
        console.log("Sending data to astream for "+this.streamDestination)
        Utilities.extendData(payload, this.asr_additional_data).then(
            function (data) {
                console.log(data);
                that.streamDestinationWSMap[that.streamDestination].emit("astream", data);
            },
            function () {
                that.executeCallback("stream_error", "extending payload failed");
                that.streamDestinationWSMap[that.streamDestination].emit("astream", payload);
            }
        )
        that.lastChunkSentTime = Date.now()
    }

    changeStreamDestination(dest) {
        console.log("Changing Stream destination to "+dest);
        this.streamDestination = dest;
        
        if (!this.streamDestinationWSMap[dest]) {
            throw dest+" Stream is not not defined";
        }

        if (dest == "asr") {
            this.stream_type = this.asr_type
        } else if(dest =="wakeup") {
            this.stream_type = this.wakeup_type            
        }

        //Implement image strem events here
    }

    executeCallback(event, data) {
        try {
            this.event_callback(event,data)
        } catch (e) {
            console.log(event, data);
            console.log("Executing callback failed.");
            console.error(e);
        }
        
    }

    initASREvents() {
        let that = this;
        this.streamDestinationWSMap.asr.on('connect', function(data) {
            that.executeCallback("asr-connect", "ASR Websocket connection established.");
        });

     
        this.streamDestinationWSMap.asr.on('disconnect', function () {
            that.executeCallback("asr-disconnect", "ASR Websocket connection is terminated.");
        });
        
        this.streamDestinationWSMap.asr.on('results', function (data) {
            if(that.recognition_sid != data["recognition_sid"]) return;
            
            // console.log(data);
            that.executeCallback("asr-results", data);
            that.stopPolling(that.audio_results_polling_manager);

        });
        
        this.streamDestinationWSMap.asr.on('intermediateResults', function(data) {
            console.log("ASR from speech server: ");
            // console.log(data);
        
            if(that.recognition_sid != data["recognition_sid"]) return;

            if (data["is_final"] == true) {
                console.log("Final data is received. Stopping polling.");
                that.executeCallback("asr-results",data);
                
                that.stopPolling(that.audio_results_polling_manager);
                // if (!that.vadEnabled) {
                // }
            } else {
                that.executeCallback("asr-intermediate-results",data);
            }            
        });
        
        this.streamDestinationWSMap.asr.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("asr-error", data);
            }
        });

        this.streamDestinationWSMap.asr.on('recText', function(data){

            if(typeof data != "object"){
                data = JSON.parse(data);
            }

            if(that.recognition_sid != data["recognition_sid"]) return;
            // console.log("recText: "+data);
            that.executeCallback("asr-recText", data);
            that.stopPolling(that.audio_results_polling_manager, false);
        });
        
        this.streamDestinationWSMap.asr.on('convResp', function(data){
            console.log("convResp:");
            console.log(data);
            
            if(typeof data != "object"){
                data = JSON.parse(data);
            }

            if(that.recognition_sid != data["recognition_sid"]) return;
            try {
                if(typeof data == "object"){
                    that.executeCallback("asr-convResp",data["conv_resp"] || data["conversation_api_response"] || {});
                }else{
                    that.executeCallback("asr-convResp",data);
                }
            } catch (e) {
                console.error(e);
            }
            that.stopPolling(that.audio_results_polling_manager);
            that.recognition_sid = undefined;
        });
    }

    initWakeupEvents() {

        let that = this;

        this.streamDestinationWSMap.wakeup.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("wakeup-error", data);
            }
        });
        
        
        
        this.streamDestinationWSMap.wakeup.on('hotword', function(data) {
            console.log(data);
            that.detectWakeup = false;
            that.stopPolling(that.audio_results_polling_manager);
            that.executeCallback("wakeup-hotword", data)
            // that.changeStreamDestination("asr");
            if (!that.auto_asr_detect_from_wakeup) {
                that.stopAudioRecording(this.conv_params, false)
            } else {
                that.stopAudioRecording(this.conv_params, false)
                that.start_asr()
            }
        });
    }

    initImageEvents() {
        let that = this;
        this.streamDestinationWSMap.image.on('connect', function(data) {
            that.executeCallback("imagews-connect", "Websocket connection to image server established.");
        });

     
        this.streamDestinationWSMap.image.on('disconnect', function () {
            that.executeCallback("imagews-disconnect", "Websocket connection to image server is terminated.");
        });

        this.streamDestinationWSMap.image.on('intermediateFaceDetectorResults', function(xe){
            try {
                console.log(xe);
            } catch(e) {
                console.error(e);
            }
        });

        this.streamDestinationWSMap.image.on("intermediateClassifierResults", function(e){
            try {
                // console.log(e);
                that.executeCallback("imagews-results", e);
            } catch(e) {
                console.error(e);
            }
        });
    }

    initSocket() {


        // this.changeStreamDestination("wakeup");

        this.uid = Utilities.makeid(6);

        console.log(`Setting up Socketio.`);
     
        if (this.image_processing && this.image_server) {
            console.log("Initng Image server.");
            this.streamDestinationWSMap.image = io(this.image_server, {query:"uid="+this.uid});
            this.initImageEvents();
            this.streamDestination = "image";
        }
        
        if (this.wakeup && this.wakeup_server) {
            this.streamDestinationWSMap.wakeup = io(this.wakeup_server, {query:"uid="+this.uid});
            this.initWakeupEvents();
            this.streamDestination = "wakeup";
        }
        
        if (this.asr && this.asr_server) {
            this.streamDestinationWSMap.asr = io(this.asr_server, {query:"uid="+this.uid});
            this.initASREvents();
            this.streamDestination = "asr";
        }
    }
    
    start_asr() {
        if (this.asr == false) {
            if (this.detectWakeup) {
                console.log("Wakeup is not detected yet.");
                return;
            }
        }
        
        this.changeStreamDestination('asr');
        this.executeCallback("asr-recording", "recording");
        this.startAudioRecording();
    }

    check_is_recording() {
        // console.log("record check "+this.is_recording);
        if (this.is_recording == true) {
            return true;
        } else {
            return false;
        }
    }
    //Polling functions.
    //This function polls wesocket server for intermediate results every 2 seconds.
    createPolling(uid, recognition_sid, interval = 2000) {
        var that = this;
        var poll_counter = 50;
        console.log("Creating polling (for "+that.streamDestination+")");

        const intervalObject = setInterval(function() {
            // console.log(that.check_is_recording());
            if (that.check_is_recording() == false) {
                poll_counter = poll_counter - 1;
                console.log("Polling ends after "+poll_counter+" trials.")
                if (poll_counter <= 0) {
                    that.stopPolling(that.audio_results_polling_manager);
                }
            }
            // console.log("This messsage gets printed every 2 seconds.");
            if (that.streamDestination == "wakeup") {
                console.log("Polling for wakeup.");
                that.streamDestinationWSMap["wakeup"].emit("hotwordResults",{"uid":uid,"recognition_sid":recognition_sid});
            } else {
                console.log("Polling for asr.");
                that.streamDestinationWSMap["asr"].emit("intermediateResults",{"uid":uid,"sid":recognition_sid});
            }
        }, interval);
    
        return intervalObject;
    }

    stopPolling(intervalObject, clear = true) {
        console.log("Stopping polling..");
        clearInterval(intervalObject);
        if (clear) {
            this.recognition_sid = undefined;
        }
    }

    // startRecording(customer_id, system_response) {

    // }

    replaceAudio(src) { 
        var newAudio = document.createElement('audio');
        // var newAudio = document.getElementById("playback");
        newAudio.controls = true; 
        newAudio.autoplay = true; 
        if(src) { 
            newAudio.src = src;
        }
        
        // var parentNode = newAudio.parentNode; 
        // newAudio.innerHTML = ''; 
        // newAudio.appendChild(newAudio); 
        AUDIO = newAudio; 
    }

    setupRTC() {
        var that = this;
        console.log("Setting up RTC.");
        // if (that.vad) {
        //     this.setupVAD();
        // }
        this.startAudioRecording = function(conv_params = {}) {
                console.log("Record")
                
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';  
    
                that.is_recording = true;
                navigator.getUserMedia(
                    {audio: true}, 
                    function(stream) {
                        console.log("Active Stream set.");
                        that.activeStream = stream;
                        that.executeCallback(that.streamDestination+"-recording", "recording");
                        that.recordAudio = RecordRTC(
                            stream, 
                            {
                                type: 'audio',
                                mimeType: 'audio/wav',
                                sampleRate: 44100,
                                timeSlice: 2000,
                                bufferSize : 1024,
                                recorderType: StereoAudioRecorder,
                                numberOfAudioChannels: 1,
                            
                                ondataavailable: function(blob) {
                                    
                                    // console.log(that.stream_type);
                                    if (!that.recognition_sid) {
                                        that.recognition_sid = Utilities.makeid(8);
                                        that.audio_results_polling_manager = that.createPolling(that.uid, that.recognition_sid);
                                    }

                                    if (that.stream_type == "full") {
                                        that.BLOB = blob;
                                    } else if (that.stream_type == "chunks") {

                                        
                                        // console.log("ASR CHUNKS");
                                        conv_params = {...conv_params, ...that.const_params, ...{
                                            "size":blob.size,
                                            "blob":blob, 
                                            "recognition_sid":that.recognition_sid, 
                                            "is_recording":that.is_recording,
                                            "timestamp": Utilities.getTime(),
                                            "recognizer" : that.recognizer,
                                            "model_name" : that.model_name
                                        }};
                                        
                                        
                                        // console.log(payload);
                                        
                                        that.cacheAndSendData(conv_params);
                                        // this.socketio.emit('astream', audio_payload);
                                    }
                                }
                            }
                        );
                        
                        that.recordAudio.startRecording();
                        // this.stopRecordingButton.style.display = 'block';
                    }, 
                    function(error) {console.error(JSON.stringify(error));}
                );
        };
    
        this.stopAudioRecording = function(conv_params={}, call_to_backend = true) {
            console.log(`Call to backedn ${call_to_backend}.`)
            if(that.micAccess){
                console.log("Stopping stream to ASR.");
                // console.log("Conversation Params");
                // console.log(conv_params);
                // recording stopped
                // this.startRecordingButton.style.display = 'block';
                // this.stopRecordingButton.style.display = 'none';
                                
                that.is_recording = false;

                if (call_to_backend) {

                    conv_params = { ...conv_params, ...that.const_params, ...{
                        "size":0,
                        "blob":"", 
                        "recognition_sid":that.recognition_sid, 
                        "is_recording":that.is_recording,
                        "timestamp": Utilities.getTime()
                    }};


                    
                    if (that.stream_type == "chunks") {
                        that.executeCallback(that.streamDestination+"-processing", "processing");
                        that.streamDestinationWSMap[that.streamDestination].emit('astream', conv_params);
                    }
                }
                
                if (!that.recordAudio) {
                    console.log("Audio recording already stopped.");
                    return;
                }
                //// stop audio recorder
                that.recordAudio.stopRecording(function() {
                    console.log("Stopping recording.");
                    if (that.stream_type == "full" && call_to_backend) {

                        // replaceAudio(URL.createObjectURL(that.BLOB));
                        // AUDIO.play();
                        // after stopping the audio, get the audio data
                        that.recordAudio.getDataURL(function(audioDataURL) {
                            if (call_to_backend) {
                                
                                var payload = { ...conv_params, ...that.const_params, ...{
                                    "size":0,
                                    "recognition_sid":that.recognition_sid, 
                                    "is_recording":that.is_recording,
                                    "timestamp": Utilities.getTime(),
                                    "recognizer" : that.recognizer,
                                    "model_name" : that.model_name
                                }};

                                if (that.recognizer == "google") {
                                    // console.log("Sending data for google recognition via flacking channel.");
                                    // that.onWavLoad(files.audio.dataURL.split(",")[1], selectedRecognizer);
                                    Utilities.doFLAC(audioDataURL.split(",")[1]).then(
                                        function (encData, metaData) {
                                            // console.log(encData);

                                            let blob = exportFlacFile(encData, metaData, false);
                                            // var fileName = getFileName("flactest", isUseOgg()? 'ogg' : 'flac');
                                            let reader = new FileReader();
                                            reader.onload = function() {
                                                
                                                payload.blob = reader.result;
                                    
                                                // console.log(payload);
                                                that.streamDestinationWSMap["asr"].emit("flacking", payload);
                                                that.executeCallback(that.streamDestination+"-processing", "processing");
                                                that.killAudioTracks();
                                            }

                                            reader.readAsDataURL(blob);

                                        },
                                        function (messageString, error) {
                                            console.log("There is an error.");
                                            console.log(messageString);
                                            console.log(error);
                                            that.executeCallback("asr-error", "FLAC encoding failed");
                                        }
                                    )
                                } else {
                                    payload.blob = audioDataURL;
                                    that.executeCallback(that.streamDestination+"-processing", "processing");
                                    that.streamDestinationWSMap["asr"].emit('stt', payload);
                                    that.killAudioTracks();
                                }
                            } 
                            
                        });
                    } else {   
                        that.killAudioTracks();
                    }

                });

            }
        };
    }

    getMedia(requests, callback){
        var that = this;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia;
        
        if(navigator.getUserMedia){
            if(that._request_pending){
                that.executeCallback("interaction", InteractionStageEnum.MIC_REQUESTED);
            }
            navigator.getUserMedia(requests, function(stream) {
                console.debug("Cool:::Got auth for audio input");
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
                    that._request_pending=false;
                }
                callback(stream);
            }, function(error) {
                console.log("error while mic init");
                console.log(error);
                if(that._request_pending){
                    that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                    that._request_pending=false;
                }
                callback(false, error);
                that.mediaFailed();
            });
        }else{
            callback(false);
        }
    }

    mediaPermissions(){
        //class Streamer
        var that = this;
        function callbacks(sts){
            that._request_pending = false;
            if(sts){
                that.executeCallback("interaction", InteractionStageEnum.MIC_ALLOWED);
            }else{
                that.executeCallback("interaction", InteractionStageEnum.MIC_REJECTED);
                that.mediaFailed();
            }
        }
        // this.getMedia({audio: true}, callbacks);
        
        if(navigator.permissions){
            navigator.permissions.query({name:'microphone'}).then(function(result) {
                console.debug(result.state)
                if(result.state == "denied"){
                    callbacks(false);
                }else if(result.state == "granted") {
                    callbacks(true);
                }
               });
        }
    }

    killAudioTracks() {
        if (this.vadEnabled) {
            this.vadSource.disconnect()
            this.vadobj.scriptProcessorNode.disconnect()
            this.vadobj = undefined;
            this.vadEnabled = false;
        }
        
        if (this.activeStream) {
            console.log(this.activeStream.getTracks());
            let tracks = this.activeStream.getTracks();
            tracks.forEach(track => {
                console.log("Killing track.");
                track.stop();
            });
        }
        this.recordAudio = undefined;
        this.activeStream = undefined;
    }

    forceStopRecording() {
        this.stopAudioRecording({},false);
        this.killAudioTracks();
        this.stopPolling(this.audio_results_polling_manager);
    }
    forceStopWakeup = this.forceStopRecording;

    cancelRecordStopTimer(id) {
        clearTimeout(id);
    }   

    startRecordStopTimer(T) {
        this.recordTimer = setTimeout(function() {
            console.log("Record timout stopping recoding.")
            this.stopVoiceRecording();
        }, T * 1000);
    }
    
    captureImage() {
        let image = this.cc.captureImage();
        return image;
    }

    stopImageResultsPolling() {
        console.log("Stopping image results polling.");
        clearInterval(this.image_results_polling_manager);
    }

    startImageResultsPolling() {
        let that = this;
        this.image_results_polling_manager = setInterval(function() {
            console.log("Polling for image classifier results.")
            that.streamDestinationWSMap.image.emit("intermediateClassifierResults", {"uid":that.uid, "sid":that.image_recognition_sid});
        }, 1000);
    }

    start_video_capture(frames_to_capture = undefined , subsampling_factor = undefined, img_data = undefined) {
        this.is_video_recording = true	
        this.cc.cameraInit("vid");
        var that =  this;
        if (!this.image_recognition_sid) {
            console.log("Generating image rsid.");
            this.image_recognition_sid = Utilities.makeid(8)
            this.startImageResultsPolling();
        }

        function _post(img_data){
            console.log("Sending image to server.");
            let imageData = img_data;
            console.log(that.image_classifier);
            let payload = '[enterprise_id][conversation_id][customer_id][engagement_id][api_key][system_response][timestamp]||||{"recognizer_type":"selectedRecognizer", "classNames":["'+that.image_classifier+'"]}||||'+imageData+'||||';
            that.streamDestinationWSMap.image.emit("imageStream",{"uid":that.uid,"sid":that.image_recognition_sid,"is_recording":that.is_video_recording, "data":payload});

        }
        if(!img_data){
            this.image_capture_manager = setInterval(function() {
                if (frames_to_capture == 0) {
                    that.executeCallback("imagews-capture_ended", "capture_ended");
                    that.stop_video_capture();
                    return
                } else {
                    console.log(`Will capture ${frames_to_capture} more frames.`)
                    frames_to_capture = frames_to_capture - 1;
                }
                _post(that.captureImage());	
            }, 1000/this.fps);
        }else{
            _post(img_data)
        }    
        that.executeCallback("image-recording", "recording");
    }

    stop_video_capture() {
        clearInterval(this.image_capture_manager);
        this.is_video_recording = false;
        this.cc.stopMediaTracks(this.cc.currentMediaStream)
        this.stopImageResultsPolling();
        this.image_recognition_sid = undefined; 
        this.executeCallback("image-stoprecording", "stoprecording");
    }

    startVoiceRecording(conv_params){
        //if any audio/video stream is ON just kill them
        if (this.vad && !this.vadEnabled) {
            this.setupVAD();
            this.vadEnabled = true;
        }
        this.conv_params = conv_params;
        
        this.changeStreamDestination("asr");
        this.startAudioRecording(conv_params)
        console.log(this.audio_auto_stop);
        // this.startRecordStopTimer(this.audio_auto_stop);
    }
    stopVoiceRecording(){
        this.stopAudioRecording(this.conv_params)
    }
    startWakeupRecording(conv_params){
        //if any audio/video stream is ON just kill them
        //same as startAsrRecording.
        this.conv_params = conv_params;
       
        this.changeStreamDestination("wakeup");
        this.startAudioRecording(conv_params)
    }
    stopWakeupRecording(){
        this.stopAudioRecording({}, false)
    }

    startVideoRecording(){
        this.start_video_capture(this.frames_to_capture, this.subsampling_factor)
    }
    stopVideoRecording(){
        this.stop_video_capture()
    }
} 
class Conversation extends DaveService {
    constructor(host, enterprise_id, conversation_id, settings, onSetupDone=null, onSetupError=null, onAction=null) {
        //class Conversation
        super(host,enterprise_id, settings);
        var that = this;
        this.enterprise_id = enterprise_id;
        this.conversation_id = conversation_id;
        this.host_url = host;
        
        this.voice_id = settings["voice_id"];
        this.customer_id = this.user_id;
        this.conversation_params = settings["conversation_params"];
        
        this.conversation_data = {};
        this.conversation = {};
        this.current_response = {};
        this.settings = settings;

        this._loading_conversation = true;
        this._loading_conversation_keywords = true;
        
        // this.streamer = new Streamer(that.host_url, that.enterprise_id, that.settings, function(s, m){
        //     that.eventCallback(s, m);
        // });

        var streamer_settings = {
            wakeup : settings["wakeup"]?settings["wakeup"]:(settings["wakeup_url"] ? true: false),
            asr : settings["asr"]? settings["asr"]: (settings["speech_recognition_url"] ? true: false),
            image_processing : settings["image_processing"]? settings["image_processing"]: (settings["image_recognition_url"] ? true: false),
            wakeup_server : settings["wakeup_url"], 
            asr_server : settings["speech_recognition_url"], 
            image_server :  settings["image_recognition_url"],
            image_classifier : settings["image_recognition_url"] ? "FaceDetectorCafe": undefined,  
            frames_to_capture : 30,
            recognizer : settings["recognizer_type"] || 'google', //Google || Kaldi || Reverie || ..
            model_name :"indian_english",
            conversation_id : that.conversation_id,
            enterprise_id : that.enterprise_id, 
            base_url : that.host_url,
            auto_asr_detect_from_wakeup: settings["auto_asr_detect_from_wakeup"] == undefined? false: settings["auto_asr_detect_from_wakeup"],
            vad : settings["vad"] == undefined? true: settings["vad"],
            audio_auto_stop: 10,
            asr_type :  settings["speech_recognition_type"] || "full", //full-'flacking'||"chunks"-'asteam'||"direct"-'sending-flag',
            wakeup_type : settings["wakeup_type"] || "chunks", //chunks-'asteam'||"direct"-'sending-flag',
            video_type : settings["video_stream_type"] || "chunks", //chunks-'asteam'||"direct"-'sending-flag'
            asr_additional_info : settings["additional_conversation_info"] || {},
            event_callback : function (event, data) {
                console.log(event, data);
                that.eventCallback(event, data);
            }
        }
        this.streamer = new Streamer(streamer_settings);
        
        this.onSetupDone = onSetupDone;
        this.onSetupError = onSetupError;
        this.keywords = [];
        this._req = null;
        this.event_callback = function(status, message){
            if(event_callback){
                that.eventCallback(status, message);
            }
        }
        this.onAction = onAction;
        this.loadConversation();
        this.loadKeywords();
        
    }
    changeUser(customer_id, api_key){
        this.current_response = {};
        this.customer_id = customer_id;
        this.user_id = customer_id;
        this.api_key = api_key;
        this.engagement_id = undefined;
        this.session_id = undefined;
        this.headers = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": this.enterprise_id,
            "X-I2CE-USER-ID": this.user_id,
            "X-I2CE-API-KEY": this.api_key
        }
        Utills.setCookie(this.authentication_cookie_key, JSON.stringify(this.headers), 240);
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.user_id;
        console.debug("user updated :: ", this.headers);
    }
    triggerEvents(status, message){
        if(this.onAction){
            this.onAction(status, message);
        }
    }

    post_interaction(obj){
        var that = this;
        var user_id_attr = this.settings["interaction_user_id_attr"] || this.settings["user_id_attr"]
        obj[user_id_attr] = this.customer_id;

        if(!obj[that.settings["stage_attr"] || "stage"]){
            console.debug("Interaction disabled")
            return;
        }

        extendData(obj, this.settings["additional_interaction_info"]).then(function(exdata){
            if(!exdata){
                return;
            }
            that.post(that.settings["interaction_model"], exdata, function(data){ 
                console.debug(data)
                that.eventCallback("interaction_response", data);
            }, function(err){
                console.error(err)
                that.eventCallback("interaction_response", err.error || err);
            });
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }

    startSession(){
        var that = this;
        if(!this.settings["session_model"]){
            return;
        }
        this.session_start_time = (new Date()).getTime();
        if(this.session_id){
            return
        }
        var obj = {};
        var user_id_attr = this.settings["session_user_id_attr"] || this.settings["user_id_attr"]
        
        obj[user_id_attr] = this.customer_id;
        obj[this.settings["conversation_id"]] = this.conversation_id;
        extendData(obj, this.settings["additional_session_info"]).then(function(exdata){
            that.post(that.settings["session_model"], exdata, function(data){ 
                console.debug(data)
                that.eventCallback("start_session", data);
                that.session_id = data[that.settings["session_id_attr"] || that.settings["session_model"]+"_id"] || data["session_id"];
            }, function(err){
                console.error(err)
                that.eventCallback("start_session", err.error || err);
            });
        }).catch(function(err){
            console.error(err);
        })
        //class Conversation
    }
    updateSession(async){
        if(!this.settings["session_model"] || !this.session_start_time){
            return;
        }
        var that = this;
        var diff = new Date().getTime() - this.session_start_time;
        var obj = {"_async": true, "async": async == undefined ? true : false};
        obj[this.settings["conversation_id"]] = this.conversation_id;
        obj[this.settings["session_duration_attr"] || "session_duration"] = diff/1000;
        this.session_start_time = undefined;
        this.iupdate(this.settings["session_model"], this.session_id, obj, function(data){ 
            console.debug(data)
            that.eventCallback("update_session", data);
            deleteAllCookies();
        }, function(err){ 
            console.error(err)
            that.eventCallback("update_session", err.error || err);
        });
    }
    endSession(obj){
        if(!this.settings["session_model"]){
            return;
        }
        var that = this;
        var obj = {"_async": true};
        obj[this.settings["conversation_id"]] = this.conversation_id;

        this.update(this.settings["session_model"], this.session_id, obj, function(data){ 
            console.debug(data)
            that.eventCallback("end_session", data);
            deleteAllCookies();
        }, function(err){
            console.error(err)
            that.eventCallback("end_session", err.error || err);
        });
    }
    
    startListenForWakeup(){
        this.streamer.startWakeupRecording({});
    }
    stopListenForWakeup(force){
        if(force){
            this.streamer.forceStopWakeup()
        }else{
            this.streamer.stopWakeupRecording();
        }
    }
    startImageStream(){
        this.streamer.startVideoRecording();
    }
    stopImageStream(){
        this.streamer.stopVideoRecording();
    }
    startRecordResponse(){
        // this.eventCallback("recording");
        // this.streamer.startRecordResponse(null, false, true, this.current_response["name"]);
        this.streamer.startVoiceRecording({
            voice_id: this.voice_id || 'english-male',
            query_type: "speech",
            language: this.language || 'english',
            system_response: this.current_response["name"]
        })
    }
    stopRecordResponse(){
        // this.eventCallback("speech_processing");
        // this.streamer.stopRecordingResponse();
        this.streamer.stopVoiceRecording();
    }

    
    
    
    eventCallback(status, message){
        console.log(status, message);
        //class Conversation
        if(status == "error"){
            this.triggerEvents("idel");
            //that.precessError({});
            this.processing = false;
        }else if(status == "interaction"){
            this.post_interaction({[this.settings["stage_attr"] || "stage"]: message});
        }else if(status == "mic_rejected" || status == "socket_error"){
            this.triggerEvents("mic_rejected");
        }else if(status == "asr-convResp"){
            this.processing = false;
            this.state = "idel";
            if(this.timeout_level_3){
                clearTimeout(this.timeout_level_3);
                this.timeout_level_3 = undefined;
            }
            if(this.timeout_timeout){
                clearTimeout(this.timeout_timeout);
                this.timeout_timeout = undefined;
            }
            
            if( typeof message  === "object" ){
                var dt = message || {};
                dt["auto_recording"] = message["auto_recording"];
                if( !dt["name"] ){
                    console.log("--------------- error response", dt);
                    this.triggerEvents("conversation-error", dt);        
                }else{
                    this.triggerEvents("conversation-response", dt);
                }
            }else{
                console.debug("--------------- not a json response", message);
                this.triggerEvents("conversation-error", dt);
            }
        }else if(status == "asr-recText"){
            console.debug("level -2 callback")
            this.triggerEvents("speech-detected", message.recognized_speech || "");
            var that = this;
            that.timeout_level_3 = setTimeout(function(){
                console.debug("level -3 callback")

                if(that.processing){
                    that.triggerEvents("delay", message.recognized_speech || "");
                }
            }, 15000);

        }else if(status == "imagews-capture_ended"){
            this.triggerEvents("capture_ended", message);
        }else if(status == "imagews-results"){
            if(message["classifier_result"]){
                for(var z of message["classifier_result"]){
                    if(z["Result"] == "Face"){
                        this.triggerEvents("person_detected", message);
                        if(this.settings["auto_image_stream_stop"] != false){
                            this.stopImageStream();
                        }
                    }
                }
            }
        }else if(status == "more_delay"){
            this.triggerEvents("delay","");
        }else if(status == "timeout"){
            if(this.state != "idel" && this.processing){
                console.log("Speech reco timed out")
                this.triggerEvents("timeout", {"error": "I'm not able to get an answer for that currently. Can you try with something simpler?"});
                
                this.processing = false;
                // setTimeout(function(){
                //     that.startListenForWakeup();
                // }, 1500);
            }
        }else if(status=="wakeup-hotword"){
            console.debug("hotword detected", message);
            this.triggerEvents("hotword", message);
            if(this.settings["auto_wakeup_listern_stop"] != false){
                this.streamer.forceStopWakeup();
            }
        }
        else if(status=="asr-processing"){
            // that.socketio.emit('stt', payload);
            this.triggerEvents("speech_processing");
            this.processing = true;
            this.state = "processing";
        }
        else if(status == "asr-disconnect"){
            if(this.state == "processing"){
                this.triggerEvents("socket_disconnected", {"error": "Your Internet connection was disrupted during the query. Please try saying this again."});
                this.processing = false;
            }
            this.state = "idel";
        } else if(status == "asr-recording"){
            this.triggerEvents("recording");
            this.state = "recording";

            this.abortReq();
        }else if (status == "asr-idel"){
            this.triggerEvents("idel", message);
            this.state = "idel";
            this.processing = false;
            this.abortReq();
        } else{
            console.debug(status, message);
            this.triggerEvents(status, message)
        }
        
        
        if(["error", "mic_rejected", "socket_error"].indexOf(status) > -1){
            // if(that.record_button){
            //     that.record_button.attr("disabled", true);
            // }
        }else{
            // if(that.record_button){
            //     that.record_button.attr("disabled", false);
            // }
        }
    }
    
    abortReq(){
        //class Conversation
        if(this._req){
            this._req.abort();
        }
        
    }
    abortAsrReq(){
        this.streamer.forceStopRecording();
    }
    checkSetupDone(){
        //class Conversation
        if(!this._loading_conversation && !this._loading_conversation_keywords){
            this.onSetupDone();
        }
    }
    
    loadConversation(){
        //class Conversation
        var ref = this;
        this.get("conversation", this.conversation_id, function(data){
            ref.conversation_data = data["data"];
            ref.conversation = data;
            ref._loading_conversation = false;
            ref.checkSetupDone();
        }, function(error){
            console.log(error);
            ref._loading_conversation = false;
            ref.checkSetupDone();
            throw error;
        })
    }
    
    loadKeywords(){
        //class Conversation
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        
        var ref = this;
        this.getRaw("/conversation-keywords/"+ref.conversation_id, function(da){
            ref.keywords = da["keywords"] || [];
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        }, function(res){
            var msg;
            if(res.responseJSON){
                msg = "Looks like the system is under maintenance, I've been asked to say '"+res.responseJSON["error"]+"'";
            }else{
                msg = "Sorry, it seems like my team is tinkering around with me!! |I should be back online and ready to talk to you once this update is done..."
            }
            console.log(msg);
            ref._loading_conversation_keywords = false;
            ref.checkSetupDone();
        });
    }
    filterStates(kwd){
        //class Conversation
        if(!kwd) return this.keywords;
        function match_score(l, k){
            var x= l.filter(function(i){
                return i.toLowerCase().indexOf(k.toLowerCase()) != -1 ? true : false;
            });
            return x.length;
        }
        var keys = kwd.split(" ");
        keys.push(kwd);
        for(var i in this.keywords){
            var c = 0;
            for(var j in keys){
                c += match_score([this.keywords[i][0], this.keywords[i][1], this.keywords[i][2]], keys[j]);
                c += match_score(this.keywords[i][4], keys[j]);
            }
            this.keywords[i].score = c;
        }
        var x = this.keywords.filter(function(a){ return a.score; });
        return x.sort(function(a,b){ b.length - a.length });
    }
    
    getStateKeywords(state){
        //class Conversation
        for(var i of this.keywords){
            if(i[1] == state)
            return i
        }
        return [];
    }

    postConversationFeedback(rating, cb){
        if(rating <= 5 && rating >=1){
            this.updateRaw(`/conversation-feedback/dave/${this.engagement_id}`, {
                "usefulness_rating": rating
            }, function(data){
                if(cb) cb(data)
            }, function(e){
                if(cb) cb(e)
            })
        }
    }
    
    start(successCallback, errorCallback){
        var that = this;
        //class Conversation
        // this.customer_id = customer_id;
        this.conversation_url = "/conversation/"+this.conversation_id+"/"+this.customer_id;
        extendData({"async": false, "query_type": "auto"}, this.settings["additional_conversation_info"]).then(function(data){
            that.getNext(data, null, function(data){
                successCallback(data)
                that.init_response = data;
            }, errorCallback);
        }).catch((err) => console.log("Failed to extend request object", err))

        
        this.streamer.set("customer_id", this.customer_id);
        this.streamer.set("api_key", this.api_key);
        // this.streamer.setupStreamers(this.conversation_id, this.customer_id, this.api_key);
    }

    getNext(data, title, successCallback, errorCallback){
        //class Conversation
        var async_ = data["async"] || true;
        // delete data["async"];
        
        var ref = this;
        if(this.engagement_id){
            data["engagement_id"] = this.engagement_id;
        }
        if(this.voice_id){
            data["voice_id"] = this.voice_id;
        }
        if(this.current_response){
            data["system_response"] = this.current_response["name"];
        }
        this.abortReq();

        data["synthesize_now"] = true;
        data["csv_response"] = true;
        
        console.debug("data: ", data);
        
        if(data["customer_response"]){
            var rx = title|| data["customer_response"];
            this.eventCallback("processing",  rx.indexOf("{") == -1 ? rx : (data.title || "Form response"));
        }
        this._req = this.postRaw(this.conversation_url, data, function(data){
            ref.engagement_id = data["engagement_id"]
            data["data"] = data["data"] || {};
            ref.voice_id = data["data"]["voice_id"] || ref.voice_id;
            ref.streamer.set("engagement_id", data["engagement_id"]);
            successCallback(data);
            ref.eventCallback("idel");
            ref.current_response = data;
            //ref.precessResponse(da, successCallback)
        }, function(error){
            errorCallback(error);
            ref.eventCallback("idel");
            //ref.processError(res, errorCallback);
        })
    }
    getNudge(state, successCallback, errorCallback){
        var ref = this;
        
        this.abortReq();
        var data = {}
        data["customer_state"] = state;
        data["system_response"] = this.current_response? this.current_response["name"]: null;
        if(this.engagement_id){
            data["engagement_id"] = this.engagement_id;
        }
        data["synthesize_now"] = true;
        data["csv_response"] = true;
        data["query_type"] = "auto";

        extendData(data, this.settings["additional_conversation_info"]).then(function(data){
            console.debug(data)
            ref.eventCallback("processing", "nudge");
            ref._req = ref.postRaw(ref.conversation_url, data, function(data){
                data["data"] = data["data"] || {};
                successCallback(data);
                ref.eventCallback("idel");
                //ref.precessResponse(da, successCallback)
            }, function(error){
                errorCallback(error);
                ref.eventCallback("idel");
                //ref.processError(res, errorCallback);
            })   
        }).catch((err) => console.log("Failed to extend request object", err))
        
    }
    
    next(response, state=null, query_type=null, title=null, successCallback=null, errorCallback=null){
        //class Conversation
        var that = this;
        var data = {};
        data["customer_response"] = response;
        if(state)
            data["customer_state"] = state;
        console.debug(this.current_response);
        data["system_response"] = this.current_response["name"];
        data["query_type"] = query_type;
        extendData(data, this.settings["additional_conversation_info"]).then(function(data){
            that.getNext(data, title, successCallback, errorCallback);    
        }).catch((err) => console.log("Failed to extend request object", err))
    }
    getKeywords(c=4, res=null){
        var kws = []
        res = res || this.current_response
        if(res){
            for(var i in res["options"]){
                kws.push([res["options"][i], undefined]);
                if(kws.length == c) break
            }
            var l = [];
            for(var i in res["state_options"]){
                var kys = this.getStateKeywords(i);
                if(kys.length == 6){
                    if(kys[5].length > 0){
                        l.push([kys[2], i]);
                    }
                    /*
                            for(var z of kys[5]){
                                if(z.indexOf("**") >= 0) continue;
                                while(z.indexOf("__") > -1)
                                z = z.replace("__", "\"")
                                x.push(z);
                            }
                            l.push(x);*/
                }
            }
            for(var j of l){
                kws.push(j);
                if(kws.length >= c){
                    break;
                }
            }
        }
        return kws;
    }
}


class WhiteBoard{
    constructor(options){
        //class Whiteboard
        this.id = options.white_board_id;
        this.ele = $("<div />");
        this.ele.addClass(options.class || 'dave-whiteboard');
        
        this.defaultBody = options.html || '';
        this.callbacks = $.Callbacks();
        this.eventCallbacks = $.Callbacks();
        
        this.ele.on("scroll", {"type": "scroll"}, function(e){
            e.data["position"] = $(this).scrollTop();
            this.fire(e);
        })
    }
    
    html(html){
        //class Whiteboard
        if(html){
            var comp = $(html); 
            var that = this;
            comp.find(".dave-video").each(function(){
                var res = {};
                var events = ($(this).attr("data-key") || "").split(",");
                //var events = ($(this).attr("data-events") || "").split(",");
                if(!$(this).attr("data-key")){
                    throw "dave-action defined without data-key";
                }
                for(var ev of events){
                    var ev_mp = ev.split(":");
                    res["key"] = ev_mp[0];
                    res["type"] = ev_mp[1];
                    var ls_t = 0;
                    $(this).bind(ev_mp[1], res, function(e){
                        if(ev_mp[1] == "timeupdate"){
                            e.data.value = $(this)[0].currentTime;
                            var t = Math.floor($(this)[0].currentTime);
                            if( t%2 == 0 ){
                                if(t != ls_t){
                                    that.fire(e, true);
                                }
                                ls_t = t;
                            }
                        }else if(ev_mp[1] == "ended"){
                            e.data.value = "ended";
                            that.fire(e, true);
                        }else if(ev_mp[1] == "pause"){
                            e.data.value = $(this)[0].currentTime;
                            that.fire(e, true);
                        }
                    });
                }
                $(this).on('click', res, function(e){
                    e.data.value = $(this).attr("data-value")
                    that.fire(e);
                });
            })
            
            comp.find(".dave-click").each(function(){
                var res = {};
                res["key"] = $(this).attr("data-key");
                res["value"] = $(this).attr("data-value");
                res["title"] = $(this).attr("data-title");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "clicked";
                $(this).on('click', res, function(e){
                    e.data.value = $(this).attr("data-value")
                    that.fire(e);
                })
            })

            comp.find(".dave-url").each(function(){
                var res = {};
                res["key"] = $(this).attr("data-key");
                res["value"] = $(this).attr("data-value");
                res["url"] = $(this).attr("href");
                if(!res["key"] && !res["value"]){
                    throw "dave-action defined without data-key or data-value";
                }
                res["type"] = "url_click";
                $(this).on('click', res, function(e){
                    e.data.value = $(this).attr("data-value")
                    that.fire(e, true);
                })
            })

            comp.find("form.dave-form").each(function(){
                $(this).on('submit', {"type": "form", "key": $(this).attr("data-key"),"title":$(this).attr("data-title")},  function(e){
                    var dt = $(this).serialize()
                    console.debug(dt);
                    var urs = new URLSearchParams(dt);
                    var obx = {};
                    var ks = urs.keys();
                    var nxt = ks.next();
                    while(!nxt.done){
                        obx[nxt.value] = urs.get(nxt.value);
                        nxt = ks.next();
                    }
                    console.debug(obx);
                    //const data = Object.fromEntries(new URLSearchParams(dt));;
                    e.data["value"] = obx;
                    that.fire(e);
                })
            })
            
            this.defaultBody = html;
            this.ele.append(comp);
        }
        return this.ele;
    }
    
    watch(callback){
        //class Whiteboard
        this.callbacks.add(callback);
    }
    watchEvents(callback){
        //class Whiteboard
        this.eventCallbacks.add(callback);
    }    
    fire(data, eventType){
        //class Whiteboard
        if(eventType){
            this.eventCallbacks.fire(data);
        }else{
            this.callbacks.fire(data);
        }
    }    
    
    unWatch(){
        //class Whiteboard
        this.callbacks.empty();
    }
}



class Message{
    constructor(message, position="left", title= "Dave.Ai", time=null){
        //class Message
        if(typeof message== "object"){
            this.message = "Form submited successfully";
        }else{
            this.message = message;
        }
        this.position = position;
        this.title = title || "";
        if(time){
            var tt = typeof time == "string" ? new Date(time) : time;
            if(getFormattedDate(new Date()) === getFormattedDate(tt)){
                this.time = tt.getHours()+":"+tt.getMinutes();
            }else{
                this.time = getFormattedDate(tt)+" "+tt.getHours()+":"+tt.getMinutes();
            }
        }else{
            this.time = "";
        }
        this.ele = $("<li class='message "+this.position+"'/>");
        this.makeElement()
    }
    
    highlight(str){
        //class Message
        str = str? str: "";
        var msg = this.message.toLowerCase();
        if(str){ 
            var ls = msg.split(str);
            msg = ls.join( "<span class='highlight'>"+str+"</span>")
        }
        this.makeElement(msg);
    }
    
    makeElement(str){
        //class Message
        str = str || this.message;
        var inner = $(`<div style="display: table;">
        <div style="display: table-row;">
        <div class="text_wrapper" style="display: table-cell">
        <div class="text">${str}</div>
        </div>
        </div>
        <p style="margin-left: 5px;display: table-row;">
        <small style="float: left; margin-right: 10px;">${this.title}</small>
        <small style="float: right;">${this.time}</small>
        </p>
        </div>`);
        if(this.position == "right"){
            inner.css("float","right");
        }
        this.ele.empty().html(inner);
        return this.ele;
    }
    html(){
        //class Message
        return this.ele;
    }
}

/**
 * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
 * @example class AvatarConversation extends ConversationHandler
 */

 class ConversationHandler{
    /**
     * @description Handler class is abstract class which has all the abstract functions which used to handle in ui, you have to extend this functanality in you type of conversation class
     * @param {object} settings settings is and object type params which should contain host_url, conversation_ids, default_conversation, 
     * @example var settings = {
     * "enterprise_id": "dave_restaurant", 
     * "conversation_id": "deployment_kiosk",
     * "event_type": "deployment",
     * "event_id": "kiosk",
     * "host_url": "https://staging.iamdave.ai",
     * "signup_apikey": "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
     * "unity_url": "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/unity/dev_empty_bg" //unity model url,
     * "type": "kiosk" //type of event,
     * "default_placeholder" : "Type here" //placeholder on input text box,
     * "recognizer_type": "google" //ASR engine name google/kaldi,
     * "session_model": "session" //session model name,
     * "user_id_attr": "customer_id" //user id attribute in the customer model,
     * "interaction_model": "interaction" // interaction model name,
     * "speech_recognition_url": "https://speech.iamdave.ai" //ASR speech server url,
     * "image_processing_url": "https://speech.iamdave.ai" //Image processing server url,
     * "avatar_id": "dave-english-male" //avatar id of the avatar need to load,
     * "voice_id": "english-male" //Speech languange,
     * "additional_session_info": {} // aditional session attributes will post to the session model ,
     * "signup_params": {} //aditional signup params to pass to while creating signup,
     * "session_time": 50 //waiting time for each session,
     * "conversationLoadedCallback": function(data){} //callback function will respond to this callback function,
     * "session_expired_callback": function(){} //Callback function for on session expired,
     * "actionCallbacks":function(data){} //will return some conversation data after every state
     * }
     */
    constructor(settings){
        this.settings = settings;
        this.authentication_cookie_key = "authentication";
        this.minimize = false;
        for(var k in settings){
            this.set(k, settings[k]);
        }

        var _cids =  this.conversation_ids || this.conversation_id;
        if(!_cids){
            throw Error("Conversation ids required");
        }
        if(typeof _cids == "string"){
            _cids = _cids.split(",");
        }
        this.conversation_ids = _cids;
        this.conversation_id = _cids.length == 0 || !this.default_conversation ? _cids[0]: this.default_conversation;
        
        this.con_map = {};
        this.messages = [];
        this.whiteboards= [];
        

        //statuses
        this.state = "idel";
        this.audio_playing = false;
        this.load__ = false;    
        this.conversation_loaded = false;
        this.loaded = false;

        this.setupEnvironment();
    }
    
    triggerEvents(action, data){
        console.debug("Triggered event for ::", action);
        if(this.actionCallbacks && data !=undefined){
            this.actionCallbacks(data, action);
        }
    }
    onLoadedCallback(){
        if(this.conversation_loaded){
            this.onResponse(this.conversation.init_response);
            if(this.conversationLoadedCallback){
                this.conversationLoadedCallback(this.conversation.conversation_data);
                this.addShortcutsButtons();
                this.loaded =true;
            }
        }
    }
    set(key, value){
        this[key]= value;
    }
    get(key){
        return this[key];
    }
    /**
     * @description This function is use change the converstion 
     * @example ds.setConversation("deployment_kiosk_kannada")
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns will returns true if the converstation id exists in conversations list sent through constructor
     */
    setConversation(conid){
        if(this.con_map[conid]){
            this.conversation_id = conid;
            this.conversation = this.con_map[conid];
            this.refreshUI();
            return true;
        }else{
            return false;
        }
    }



    /**
     * @description This function is use for updating the state of the coversation
     * @example this.evetCallback("idel", null)
     * @param {string} status type of the state
     * @param {string} conversation_id Its id of conversation which you wants to change
     * @returns null
     */
    eventCallback(status, message){
        if(status == "signup" || status == "interaction_response" || status == "start_session" || status == "end_session" || status == "hotword" || status == "person_detected" || status == "capture_ended"){
            this.triggerEvents(status, message);
            if(status == "signup"){
                this.status = status;
            }
        }
        if(!this.loaded){
            return;
        }
        
        if(status == "idel"){
            this.onIdelState();
            this.state = status;
        }else if(status == "processing") {
            if(message != 'nudge'){
                this.playDefaults("level_one");    
                this.onProcessingRequest(message);
            }
            this.state = status;
        }
        else if(status == "speech_processing"){
            this.onProcessingRequest("....");
            this.state = "processing";
        }
        else if(status == "conversation-error") {
            this.state= "idel";
            this.onError(message);
            this.state = "idel";
        }else if(status == "conversation-response") {
            this.onResponse(message);
            this.state = "idel";
        }else if(status == "speech-detected") {
            this.onSpeechDetect(message);
            this.state = status;
        }else if(status == "delay"){
            this.playDefaults("more_delay");
            this.state = "processing";
        }else if(status == "timeout"){
            this.playDefaults("timeout");
            this.onError(message);
            this.onIdelState();
            this.state= "idel";
        }else if(status == "socket_disconnected"){
            this.onError(message);
            this.state= "idel";
        }else if(status == "recording"){
            this.onRecording();
            this.state= "recording";
        } else if(status == "end_session"){
            if(this.session_expired_callback){
                this.session_expired_callback();
            }
            this.state= "session_expired";
        }else if(status == "mic_rejected"){
            this.micRejected();
            this.state= "idel";
        }
        console.debug("Event callback in converstaion handiler", status, message);
    }

    micRejected(){}
    getAuthDetails(cb){
        var that = this;
        var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        if(!ds.checkAuth()){
            that.createUser(ds, function(ds){
                cb(ds);
            });
        }else{
            cb(ds);
            that.triggerEvents("signup", "User loaded from cookies");
        }
    }

    updateNewUser(){
        var that = this;
        this.createUser(undefined, function(ds){
            that.state = "idel";
            for(var con of that.conversation_ids){
                that.con_map[con].changeUser(ds.user_id, ds.api_key)
            }
        });
    }

    setupEnvironment(){
        var that = this;
        function _load(ds){
            if(that.event_type && that.event_id){
                ds.get(that.event_type, that.event_id, function(evdata){
                    that.settings = $.extend(evdata, that.settings);
                    that.setUpConversationUI();
                    that.loadConversations(evdata);
                }, function(error){
                    console.log(error.message || error);
                    that.triggerEvents("failed", error);
                })
            }else{
                that.setUpConversationUI();
                cb(data);
            }    
        }
        this.getAuthDetails(function(ds){
            _load(ds)
        })
    }
    postConversationFeedback(rating, cb){
        this.conversation.postConversationFeedback(rating, cb)
    }
    createUser(ds,cb){
        var that = this;
        if(!ds){
            ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        }
        var ob = this.settings["signup_params"] || this.settings["additional_signup_info"] || {};
        extendData({}, ob).then(function(sdata){
            if(that.settings["email_attr"]){
                var em_att = that.settings["email_attr"];
                sdata[em_att] = "dinesh+"+makeid(10)+"@i2ce.in";
            }
            sdata["validated"] = true;
            ds.signup(sdata, function(data){
                console.log("Signup done", data);
                that.customer_id = ds.user_id;
                that.headers = ds.headers;
                that.triggerEvents("signup", data);
                cb(ds);
            }, function(error){
                console.log("Signup failed", error);
                that.triggerEvents("signup", error.error || error);
            })
        }).catch(function(err){
            console.error("Signup failed", err);
        })
    }
    patchUser(data, success, error){
        var ds = new DaveService(this.host_url, this.enterprise_id, this.settings);
        console.debug(ds);
        ds.update(ds.customer_model_name, ds.customer_id, data, success, error);
    }

    /**
     * @description This function is use for loading all the conversations
     * @example //internal function will get called from constructor
     * @returns returns through information through 'onSignup' callback
     */
    loadConversations(){
        var that = this;
        
        var loadedcount = 0;
        for(var con of this.conversation_ids){
            (function(con){
                that.con_map[con] = new Conversation(that.host_url, that.enterprise_id, con, that.settings, function(){
                    console.log(con+" Conversation loaded");
                    // that.customer_id = that.con_map[con].user_id;
                    that.load_status = "initcon";
                    that.con_map[con].start(function(data){
                        // that.con_map[con].init_response = data;
                        
                        loadedcount++;
                        if(loadedcount == that.conversation_ids.length){
                            that.conversation_loaded = true;
                            that.onLoadedCallback();
                        }
                    }, function(error){
                        that.con_map[con].init_response = error;
                        console.error(error);
                        loadedcount++;
                        if(loadedcount == that.conversation_ids.length){
                            that.conversation_loaded = true;
                            that.onLoadedCallback();
                        }
                    }, function(s, m){
                        that.eventCallback(s, m);
                    });
                }, function(error){
                    console.error(con+" Conversation loading failed ", error);
                }, function(s, m){
                    that.eventCallback(s, m);
                } );
                if(that.conversation_id == con){
                    that.conversation = that.con_map[con];
                }
            })(con);
        }        
    }

    /**
     * @description abstract function will get call once the conversation is loaded
     */
    setUpConversationUI(){
        if(this.settings["default_avatar_id"])
            defaul_avatar_id = this.settings["default_avatar_id"];
        if(this.settings["default_bg"])
            default_bg = this.settings["default_bg"];
    }
    
    //conversation post functions
    playDefaults(state, text){
        // 1 2 3 level_one level_two level_three
        // 4 timeout
        // 0 speech_is_none
        var that = this;
        //do something
        
        if(state == "level_one" ){
            this.play_followups = [];
            that.setPlaceholder("Loading...", "left")
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
        }

        if(text){
            // that.onProcessingRequest(text);
            that.user_reponse = {"customer_response": text};
        }
    }

    
    /**
     * @description This function is for posting conversation which will call backend apis
     * @example //ds.postConversation("i am looking for a suv car", null, null);
     * @example //ds.postConversation("yes", "cs_confirm", "Yes");
     * @param {string} text customer response the reponse you got from the user
     * @param {string} state_ is optional which represents the customer_state in the conversation
     * @param {string} title title to show while processing
     * @returns returns through callbacks
     */
     postConversation(text, state_=null, title=null, query_type=null){
        var that = this;
        if(!(text || state_) || this.state != "idel" || this.load__ || this.audio_playing){
            return;
        }
        if(this._timer){
            clearTimeout(this._timer);
        }
        this.user_reponse = {"customer_state": state_};
        
        // this.onTalkingState();
        
        this.state = "processing";
        this.play_followups = [];
        this.audio_playing = false;
        this.load__ = true; 
        
        title= title ? title: text;
        // this.onProcessingRequest((title.indexOf("{") == -1 ? title : "Form response"));
        //this.toggle_response_panel();
        //$("audio").each(function(){ $(this).remove(); })
        // this.mute();
        // this.playDefaults("level_one", "", that);
        this._level_two_timer = setTimeout(function(){
            if(that.load__){
                that.playDefaults("level_two","", that);
            } 
        }, 8000)
    
        this.setWhiteboard();
        // this.conversation.hold(true);
        var m = new Message(text, "right", "You");
        this.addMessage(m, "left")
        //$("#whiteboard").append(loading_whiteboard.html())
        this.setPlaceholder("Loading...", "left");
        this.conversation.next(text, state_, query_type, title, function(data){that.onResponse(data)}, function(data){that.onError(data)});
        // this.setPlaceholder("Loading....");   
    }
    playNudge(state){
        var that = this;
        this.conversation.getNudge(state, function(data){
            that.nudgeResponse(data);
        }, function(error){
            // that.nudgeResponse(data);
            console.error(error);
        })
    }

    //onresponse ui actions
    whiteBoardActions(event, event1){
        var data = event.data;
        if(!data && event1){
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // that.setPlaceholder(m)
        this.addMessage(m);
        if( typeof data.value == "object" ){
            data.value = JSON.stringify(data.value);
        }
        console.debug(data.value, data.key);
        this.triggerEvents(data["type"], data);
        this.postConversation(data.value, data.key, data.title, "click");
    }
    whiteBoardEvents(event, event1){
        var data = event.data;
        if(!data && event1){
            data = event1.data;
        }
        var m = new Message(data.value, "right", "You");
        // this.setPlaceholder(m)
        this.addMessage(m);
        if( typeof data.value == "object" ){
            data.value = JSON.stringify(data.value);
        }
        this.triggerEvents(data["type"], data); 
        this.conversation.postEvent(data.key, data.value);
    }
    setWhiteboard(response){
        var that=this;
        
        if(!this.whiteboard_id){
            return;
        }
        
        if(this.whiteboard_ov){
            this.wb_h = $(".dave-whiteboard").css("height");
            this.whiteboard_ov.remove();
            this.whiteboard_ov = null;
        }
        if(!response){
            var loading_whiteboard = this.settings["loading_whiteboard"] || this.conversation.conversation_data['loading_whiteboard'];
            if ( loading_whiteboard ) {
                this.whiteboard_ov = $('<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + loading_whiteboard + '" style="width: 100%;"></div></div></div></div>');
                $("#"+this.whiteboard_id).append(this.whiteboard_ov);
            }
        }else{
            var wb = response["whiteboard"] ;
            if(wb){
                var w = new WhiteBoard({"white_board_id": response["name"]});
                w.html(wb);
                w.watch(function(e, e1){ that.whiteBoardActions(e, e1)});
                w.watchEvents(function(e, e1){ that.whiteBoardEvents(e, e1)});

                this.whiteboard_ov = $("<div/>");
                this.whiteboard_ov.append(w.ele);

                $("#"+this.whiteboard_id).append(this.whiteboard_ov);
                this.whiteboards.push(w.ele);
            }else{
                wb = this.settings["default_whiteboard"] || this.conversation.conversation_data['default_whiteboard'];
                if(wb){
                    this.whiteboard_ov = $('<div data-id="loading"><div class="dave-whiteboard"><div class="options-panel image"><div><img src="' + (wb) + '" style="width: 100%;"></div></div></div></div>');
                    $("#"+this.whiteboard_id).append(this.whiteboard_ov);
                }
            }
        }
        if(this.wb_h){
            $(".dave-whiteboard").css('height', this.wb_h);
        }
    }
    addMessage(m, pos="left"){
        this.messages.push(m);
        // $("#messages").html(m.html());
        //     $(".messages-div").first().animate({
        //         scrollTop: $(".message").last().offset().top+80
        //     })
    }

    setPlaceholder(m, pos, title, time){
        if(typeof m == "string"){
            m = new Message(m, pos, title || m, time);
            this.messages.push(m);
        }
        if(this.message_id){
            $("#"+this.message_id).fit_text(m.message);
        }
    }
    updateQuickAccessButtons(res){
        if(this.quick_access_id){
            $("#"+this.quick_access_id).empty();
            var that =this;
            var kws = this.conversation.getKeywords(6, res);
            for(var i of kws){
                var butn = $("<button />");
                butn.addClass("butn butn-dark butn-sm");
                butn.html(i[0]);
                butn.attr("value", i[0]);
                // butn.attr("value", i);
                butn.attr("key", i[1]);
                butn.attr("style", "margin-right: 5px");
                $("#"+this.quick_access_id).append(butn);
                butn.click(function(){
                    console.debug($(this).attr('value') || $(this).attr('key'), $(this).attr('key'), $(this).attr("title"));
                    that.postConversation($(this).attr('value'), $(this).attr('key'), $(this).attr('title'), "click");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                    that.triggerEvents("interaction", "click");
                });
            }
        }
    }
    addShortcutsButtons(){
        if(this.conversation.conversation_data["buttons"] && this.shortcuts_id){
            var sbs = this.conversation.conversation_data["buttons"] || [];
            var that = this;
            $("#"+this.shortcuts_id).empty();
            for(var i of sbs){
                var butn = $("<button />");
                butn.addClass("butn butn-dark butn-sm");
                butn.html(i["title"]);
                butn.attr("key", i["customer_state"]);
                butn.attr("value", i["customer_response"]);
                butn.attr("title", i["title"]);
                butn.attr("style", "margin-right: 5px");
                $("#"+this.shortcuts_id).append(butn);
            }
            $("button[key]").each(function(){
                $(this).click(function(){
                    console.debug($(this).attr('value') || $(this).attr('key'), $(this).attr('key'), $(this).attr("title"));
                    that.postConversation($(this).attr('value') || $(this).attr('key'), $(this).attr('key'), $(this).attr("title"), "click");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.CLICK});
                    that.triggerEvents("interaction", "click");
                });

            })
        }
    }
    onSpeechDetect(text){}
    onRecording(){}
    refreshUI(rs){
        rs = rs ? rs :this.conversation.init_response;
        if(!rs){ return };
        
        this.setPlaceholder(rs["placeholder"]);
        this.setWhiteboard(rs["whiteboard"]);
        this.updateQuickAccessButtons(rs);
        this.addShortcutsButtons();
    }

    //on response callbacks
    onResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "speaking";
        //this.toggle_response_panel(true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response) == 0) return;

        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }

        if(response["speech_is_none"]){
            that.playDefaults("speech_is_none");
            this.onIdelState(true);
            return;
        }else if(response["error"]){
            this.processSystemResponse({
                data: {},
                placeholder: response["error"]
            });
        }else{
            this.processSystemResponse(response);
        }

        
        // set_cc();
        //if(this.temp && this.temp["name"] == response["name"] && this.temp["placeholder"] == response["placeholder"] && !force){
        
        this.updateInput("");
        this.triggerEvents("conversation", response["data"]);
        
    }
    onError(error){
        this.load__ = false;
        if(this._level_two_timer){
            clearTimeout(this._level_two_timer);
            this._level_two_timer = null;
        }
        this.state = "idel";
        this.audio_playing = false;
        this.onIdelState(true);
        this.setPlaceholder(error.error || "Failed to process your request. Please try again.");
        if(this.conversation.current_response && this.conversation.current_response["name"] == error["name"]){
            if(this.user_reponse && this.user_reponse["customer_response"] && this.user_reponse["customer_response"].indexOf("{") == -1){
                $("#"+that.textbox_id).val(this.user_reponse["customer_response"]);
            }
        }
    }
    nudgeResponse(response){
        var that =this;
        this.load__ = false;
        this.state = "idel";
        //this.toggle_response_panel(true);
        console.debug(response)
        // this.conversation.hold(false);
        if(!response || Object.keys(response) == 0) return;

        var text = new Message(response["placeholder"]);
        $("#message").fit_text(response["placeholder"]);
        //this.toggle_response_panel(false);
        this.onTalkingState();
        
        if(response["whiteboard"] && (this.conversation.current_response["data"]["maintain_whiteboard"] || response["data"]["overwrite_whiteboard"])){
            this.setWhiteboard(response["whiteboard"]);
        }
    }

    //playing audio and action sequence
    playAnimations(){}
    playFromTheQueue(){}
    addAudioToQueue(){}
    addNudges(lst){}
    
    //run sequence of actions like animation, audio, update whiteboard, update bg ...etc
    processSystemResponse(response, after_played){
        this.setPlaceholder(response["placeholder"], "left", null);
        this.setWhiteboard(response);
        this.updateQuickAccessButtons(response);

        if(response["data"]){
            this.addNudges(response["data"]["_follow_ups"]);
        }
        if(this.actionCallbacks){
            this.actionCallbacks(response, "conversation_response");
        }
        this.status = "idel";
    }
    

    //ui states
    onTalkingState(){}
    onIdelState(repeat){}
    onProcessingRequest(text){}

    updateInput(text){
        if(this.textbox_id){
            $("#"+this.textbox_id).val(text || "");
        }
    }
    mute(){
        if(this.audio){
            this.audio.muted = true;
        }
    }
    unmute(){
        if(this.audio){
            this.audio.muted = false;
        }
    }
    pause(){
        this.audio_playing = false;
        this.mute();
        this.conversation.updateSession();
    }
    resume(){
        this.unmute();
        this.conversation.startSession();
        this.onResponse(this.conversation.current_response);
    }

    createSession(){
        console.log("Started new session for user :: ", this.customer_id);
        this.conversation.startSession();
    }
    clearSession(async){
        console.log("End of session for user :: ", this.customer_id);
        // this.conversation.endSession();
        this.conversation.updateSession(async);
        // this.triggerEvents("session_expired_callback", true);
    }

    triggerRandomEvent(delete_, only_wakeup=false){
        if(delete_){
            if(this.trigger_intervel){
                clearInterval(this.trigger_intervel);
            }
            this.initiated = false;
            return;
        }
        if(this.initiated){
            return
        }
        this.initiated = true;
        var that=this;
        this.trigger_intervel =  setInterval(function(){
            if(only_wakeup){
                that.triggerEvents("hotword","");
            }else{
                that.triggerEvents(["hotword", "person_detected"][Math.round(10000*Math.random())%2], "");
            }
            
        }, 5000);
    }

    startWakeupStream(){
        this.conversation.startListenForWakeup();
    }
    stopWakeupStream(force){
        this.conversation.stopListenForWakeup(force);
        // this.triggerRandomEvent(true);
    }

    startImageStream(){
        this.conversation.startImageStream();
    }
    stopImageStream(){
        this.conversation.stopImageStream();
    }

}


// class ChatBot extends ConversationHandler{}
class AvatarConversation extends ConversationHandler{
    constructor(settings){
        super(settings);
        this.avatar_loaded = false;
        this.audios_list = [];
        this.pre_resp="";
    }
    setUpConversationUI(){
        super.setUpConversationUI();
        super.set("message_id", "message");
        super.set("shortcuts_id", "shortcut");
        super.set("whiteboard_id", "whiteboard");
        super.set("quick_access_id", "quick_access");
        super.set("audio_id", "audio");
        super.set("textbox_id", "input-text");
        super.set("mic_id", "record")
        super.set("sendbutton_id", "input-send");
        super.set("mute_button", "mute");

        $("body").append("<div id='dave_conversation__' class='dave_conversation' style='display:none'></div>");
        $("#dave_conversation__").setupConversationUi({placeholder:this.default_placeholder || "Type here..."});
        $("body").append(`<audio id="audio" type="audio/wav" src="" style="display: none" controls>
                   Your browser does not support the HTML5 Audio element.
                   </audio>`);
        
        this.audio = document.getElementById((this.settings["audio_id"] || "audio"));
        this.loadAvatar();
        this.setUpEvents();
    }
    mute(){
        super.mute();
        if(this.audio){
            this.audio.muted = true;
            this.audio.setAttribute("force-mute", true);
            $("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-off");
        }
    }
    unmute(){
        super.mute();
        if(this.audio){
            this.audio.muted = false;
            this.audio.setAttribute("force-mute", false);
            $("#"+this.mute_button).find("i").first().attr("class", "fa fa-volume-up");
        }
    }
    stopTalking(){
        this.audio.pause();
        this.audio_playing = false;
        try{
            this.unityInstance.SendMessage("head", "stopTalking");
        }catch(e){
            console.log("Were not able to do stop talking", e);
        }
        
    }
    pause(){
        super.pause();
        this.audio.src = undefined;
        try{
            this.unityInstance.SendMessage("head", "stopTalking");
        }catch(e){
            console.log("Were not able to do stop talking", e);
        }
        this.audios_list = [];
    }
    resume(){
        var that = this;
        // super.resume();
        $("#dave_conversation__").show(200, function(){
            // if(that.audio){
            //     that.audio.play();
            // }
            if(that.conversation.session_id){
                that.triggerEvents("resume", "resume");
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.RESUMED});
            }else{
                that.triggerEvents("opened", "opened");
                that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.OPENED});
            }
            that.createSession();
            
            that.onResponse(that.conversation.current_response);
            setTimeout(function(){
                set_cc();
            }, 500);
        });
        this.unmute();
    }
    audioRecord(stop){
        var that = this;
        if(that.intervel){
            clearInterval(that.intervel);
        }
        that.intervel = undefined;
        if(stop){
            $("#"+that.mic_id).removeClass("active");
            $("#"+that.mic_id).html('<i id="microphone" class="fa fa-microphone fa-2x"></i>');
            $(".text-wraper").first().show();
            $("#"+this.quick_access_id).show();
            $("#"+this.shortcuts_id).show();
            $("#try-saying").show();
            return;
        }else{
            that.state = "recording";
            var sec=0;
            $("#"+that.mic_id).addClass("active");
            var intv = $("<span>00:00</span>");
            $("#"+that.mic_id).find(".fa-microphone").hide();
            $("#"+that.mic_id).append(intv);
            $("#"+that.mic_id).append("<br />");
            $("#"+that.mic_id).append('<span><i class="fa fa-stop fa-2x"></i></span>');
            
            that.intervel = setInterval(function(){
                sec++;
                var min = parseInt(sec/60);
                var sc = sec%60;
                intv.html( (min < 10 ? "0"+min : min) +":"+ ( sec < 10 ? "0"+sc : sc ));
            },1000);
            $(".text-wraper").first().hide();
            $("#quick_access").hide();
            $("#shortcut").hide();
            $("#try-saying").hide();
        }
        $("#"+that.mic_id).show();
    }
    setUpEvents(){
        var that = this;
        if(this.textbox_id){
            $("#"+this.textbox_id).enter(function(val){
                if(val.trim()){
                    that.postConversation(val, null, val, "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.TEXT_INPUT});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
        if(this.sendbutton_id){
            $("#"+this.sendbutton_id).click(function(){
                if($("#"+that.textbox_id).val().trim()){
                    that.postConversation($("#"+that.textbox_id).val(), null, $("#"+that.textbox_id).val(), "type");
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: "text-input"});
                    that.triggerEvents("interaction", "text-input");
                }
            });
        }
        if(this.mic_id){
            $("#"+this.mic_id).click(function(){
                if(that.state == "idel"){
                    // that.audioRecord();
                    $(this).hide();
                    that.conversation.startRecordResponse();
                }else if(that.state == "recording"){
                    that.conversation.stopRecordResponse();
                }
            })
        }
        if(this.mute_button){
            $("#"+this.mute_button).click(function(){
                var cc_class = $(this).find("i").first().attr("class");
                if(cc_class.indexOf("fa-volume-up") == -1){
                    that.unmute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.UNMUTED});
                    that.triggerEvents("interaction", "unmuted");
                }else{
                    that.mute();
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MUTED});
                    that.triggerEvents("interaction", "muted");
                }
            });
        }
        $("#close_message").click(function(){
            $("#dave_conversation__").hide(200);
            that.pause();
            that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.MINIMIZED});
            that.triggerEvents("interaction", "minimized");
        })
    }

    onLoadedCallback(){
        if(this.conversation_loaded && this.avatar_loaded){
            this.addShortcutsButtons();
            this.conversationLoadedCallback(this.conversation.conversation_data);
            this.loaded = true;
            var that = this;
            setTimeout(function(){
                if(!that.minimize){
                    that.createSession();
                    $("#dave_conversation__").show(200, function(){
                        that.onResponse(that.conversation.init_response);
                        // set_cc();
                        that.triggerEvents("chat_started", "chat_started");
                        
                        set_cc();
                        that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.AUTO_OPENED});
                    });
                }

                $(window).on("beforeunload", function(){
                    // that.clearSession(false);
                    that.conversation.post_interaction({ [that.settings["stage_attr"] || "stage"]: InteractionStageEnum.LEAVE_PAGE});
                })
                $(window).on("unload", function(){
                    that.clearSession(false);
                })

            }, 2000)
        }
    }
    loadAvatar(){
        var that = this;
        var t = SetupEnviron.load(this.settings["unity_url"], this.settings);
        t.then(function(ut){
            that.avatar_loaded = true;
            that.unityInstance = ut;
            that.onLoadedCallback();
            // set_cc();
        }, function(err){
            console.error(err);
        });
        t.progress(function(pro){
            console.debug(pro);
        })
    }
    

    playAnimations(audo, force) {
        var that = this;
        if (!this.audios_list.length && !audo) return;
        if (!audo) {
            audo = this.audios_list.splice(0, 1);
            audo = audo[0];
        }
        try{
            this.unityInstance.SendMessage("head", "stopTalking");
        }catch(e){
            console.log("Were not able to do stop talking", e);
        }
        this.prev_audo = audo;
        this.temp_id = audo["id"];
        var tt = this.audio.cloneNode(true);
        var muted = this.audio.getAttribute("force-mute");
        this.audio.remove();
        this.audio = tt;
        this.audio.setAttribute("data-audio-id", that.temp_id);
        this.audio.setAttribute("force-mute", muted || false);
        this.audio.muted = muted || false;
        this.audio.src = audo["audio"];
        document.body.appendChild(this.audio);
        console.debug("play init id :: ", audo["id"]);
        this.audio.load();
        // this.audio.play();

        function _lis() {
            if (audo["id"] != that.temp_id && that.temp_id != that.audio.getAttribute("data-audio-id")) return;

            function playShapes_frames(id, frames, shapes) {
            if (that.forced_stop && that.forced_stop != that.audio.getAttribute("data-audio-id")) {
                console.log("We have forced stopped, so don't play:" + id)
                return;
            }
            if ((["speech-detected", "processing", "idel"].indexOf(that.state) < 0 && that.load__) && !force) {
                console.log("State is not idle: " + that.state + " or we have loaded something else " + that.load__ + " already:" + id)
                return;
            }
            if(!audo["defaults"]){
                that.onTalkingState();
            }
            if (!that.paused) {
                try{
                    that.unityInstance.SendMessage('head', 'StartTalking', JSON.stringify({
                        "frames": frames,
                        "shapes": shapes,
                        "audio_id": id
                    }))
                }catch(e){
                    console.log("Start talking failed", e);
                }
                
                that.audio_playing = true;
                // that.conversation.hold(true);
                that.playing_id = id;
                console.debug("++++++Playing ID:" + id);
            } else {
                console.debug("++++++Paused ID:" + id);
            }
            //$("#message").html(list[i][1]);
            }
            playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
        }
        this.audio.removeEventListener("canplaythrough", _lis, false);
        this.audio.addEventListener("canplaythrough", _lis, true);
        this.audio.onended= function() {
            that.audio_playing = false;
            if(that.playing_id != that.audio.getAttribute("data-audio-id")) return;
            try{
                that.unityInstance.SendMessage("head", "stopTalking");
            }catch(e){
                console.log("Were not able to do stop talking", e);
            }
            // that.conversation.hold(false);
            if(!that.conversation.processing){
                that.onIdelState();
            }
            if(that.state == "idel" && !that.load__){
                that.pre_resp = "";
            }
            if(audo["onEnds"]){
                audo["onEnds"](that, that.playing_id);
            }else{
                that.playFromTheQueue(that, that.playing_id);
            }
        }
    }

    playDefaults(state, message, that) {
        super.playDefaults(state, message);

        // eslint-disable-next-line no-redeclare
        var that = that || this;

        function __load(level) {
            $.ajax({
                "url": that.host_url + "/defaults/" + that.voice_id + "/" + level + "/" + that.enterprise_id,
                "success": function (data) {
                    that.addAudioToQueue(data["audio"], state, "defaults", function (that, pid) {
                            that.playFromTheQueue(that, pid)
                        }, null, null, data["subfolder"], false);
                }
            });
        }
        __load(state);
    }

    playFromTheQueue(that, playing_id){
        var that = that || this;
        if(that.audios_list.length){
            that.playAnimations(null, false);
            return;
        }

        if(that._timer){
            clearTimeout(that._timer);
        }
        if(playing_id != that.latest_audio_id) return;
        if(!that.play_followups.length){
            return;
        }
        (function(tm){
            var tf = function(){
                if(that.paused){
                    setTimeout(tf, tm);     
                }
                if(that.state != "idel" || that.load__) return;
                if(that.audios_list.length){
                    that.playAnimations(null, false);
                }
                else if(that.play_followups.length){
                    that.playNudge(that.play_followups[0], function(data){that.playNudge(data)});
                    that.play_followups.splice(0, 1);
                }else{}
            }
            that._timer = setTimeout(tf, tm);
        })(that.conversation.current_response["wait"] || 10000);
    }    
    addAudioToQueue(audio, id, conversation_id, onEnds, frames, shapes, subfolder, force){
        var that =this;
        this.forced_stop = false;
        var id_ = id+"___"+makeid(4);
        console.debug("audio adding id :: ", id_);
        this.latest_audio_id = id_;
        if(force){
            this.audios_list = [];
            //this.play_followups = [];
            /*if(this.audio){
                        this.audio.muted = true;
                        this.unityInstance.SendMessage("head", "stopTalking")
                    }*/
            this.forced_stop = id_;
            if(this._timer){
                clearTimeout(this._timer);
            }
        }
        if(this._timer && onEnds){
            clearTimeout(this._timer);
        }

        var aurl;
        if(audio.indexOf("http")>=0){
            aurl = audio;
        }else{
            aurl = this.host_url+"/"+audio; 
        }
        var conversation_id = conversation_id || this.conversation_id;
        var ob = {
            id: id_,
            audio: aurl,
            onEnds: onEnds,
            defaults: (conversation_id=="defaults")
        }
        this.audios_list.push(ob);

        var fil ="?voice_id="+this.voice_id;
        if(subfolder)
            fil += "&subfolder="+subfolder;

        
        function addFramesShapes(frames, shapes) {
          if (frames) {
            ob["frames_csv"] = frames;
          }
          if (shapes) {
            ob["shapes_csv"] = shapes;
          }
          if (ob["frames_csv"] && ob["shapes_csv"]) {
            if (!that.audio_playing) {
              that.playAnimations(null, force);
            }
          }
        }
    
        var furl, surl;
        if(!frames){
            furl = this.host_url+"/frames/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            $.ajax({
                "url": furl,
                "success":function(data){
                    addFramesShapes(data)
                }
            })
        }else{
            addFramesShapes(frames);
        }
        if(!shapes){
            surl = this.host_url+"/shapes/"+conversation_id+"/"+id+"/"+this.enterprise_id+fil;
            $.ajax({
                "url": surl,
                "success": function(data2){
                    addFramesShapes(null, data2);
                }
            })
        }else{
            addFramesShapes(null, shapes);
        }
    }

    nudgeResponse(response) {
        super.nudgeResponse(response);
        var that = this;
        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
            // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
            if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function (that, pid) {
                that.playFromTheQueue(that, pid)
                }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
            } else {
                this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function (that, pid) {
                that.playFromTheQueue(that, pid)
                }, null, null, null, true);
            }
            }
            //delete response["response_channels"];
        }
    }
    addNudges(lst){
        this.play_followups = lst || [];
    }
    processSystemResponse(response, after_played, partial) {
        super.processSystemResponse(response);
        this.state="speaking";
        if(partial){
            return;
        }
        // this.displayResponse();
        var that = this;
        if (response["data"]) {
          if (response["data"]["avatar_model_name"] || response["data"]["avatar_id"]) {
            try{
                that.unityInstance.SendMessage("GameManagerOBJ", "showPersona", response["data"]["avatar_model_name"] || response["data"]["avatar_id"]);
            }catch(e){
                console.log("Were not able to update persona", e);
            }
            that.voice_id = response["data"]["voice_id"] || response["data"]["avatar_id"];
            // eslint-disable-next-line
            defaul_avatar_id = response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
          }
          if (response["data"]["bg"]) {
            try{
                that.unityInstance.SendMessage("GameManagerOBJ", "setTexture", response["data"]["bg"]);
            
            }catch(e){
                console.log("Were not able to update texture", e);
            }
            // eslint-disable-next-line
            default_bg = response["data"]["bg"];
          }
    
          if (response["data"]["function"] && response["data"]["url"]) {
            try{
                that.unityInstance.SendMessage("GameManagerOBJ", response["data"]["function"], response["data"]["url"]);
            
            }catch(e){
                console.log("Were not able to send game manger function", e);
            }
          }
          if (response["data"]["redirect"]) {
            window.open(response["data"]["redirect"], '_blank');
          }
        }
        if (response["response_channels"]) {
          if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
            // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
            if (!response["response_channels"]["shapes"].endsWith(".csv")) {
              this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function (that, pid) {
                if (after_played) {
                    after_played()
                }
                that.playFromTheQueue(that, pid)
              }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
            } else {
              this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function (that, pid) {
                if (after_played) {
                    after_played()
                }
                that.playFromTheQueue(that, pid)
              }, null, null, null, true);
            }
          }
          //delete response["response_channels"];
        }
    }
    onError(error){
        super.onError(error);
        this.playDefaults("timeout");
    }

    onTalkingState(){
        var that = this;
        this.state= "speaking";
        $(".cb__list").first().empty();
        $(".cb__list").first().html('<span class="bubble typing bubble_text">....</span>');
        if(!this.conversation.processing){
            var cancel_ = $("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Stop</button>");
            $(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.stopTalking();
                that.onIdelState(true);
            });
        }
        this.toggle_response_panel();
    }
    onIdelState(repeat){
        this.load__ = false;
        this.audio_playing = false;
        this.state = "idel";
        this.audioRecord(true);
        this.toggle_response_panel(true);
        this.displayResponse();
        this.conversation.processing = false;
        this.pre_resp = "";
    }
 
    onProcessingRequest(_text) {
        // TODO animation for processing
        console.debug("Reached on processing request", _text);
        
        this.displayResponse(_text, true);
        this.toggle_response_panel();
    }

    micRejected(){}
    onRecording(){
        console.log("Started detecting");
        this.audioRecord();
        this.conversation.post_interaction({ [this.settings["stage_attr"] || "stage"]: InteractionStageEnum.SPEECH_INPUT});
        this.triggerEvents("interaction", "speech-input");
    }
    onSpeechDetect(_text) {
        //TODO stub method
        console.log("On speech detected state", _text)
        this.displayResponse(_text);
        this.toggle_response_panel();
    }

    toggle_response_panel(idel){
        if(idel){
            $("#reponse_panel").show();
            $("#reponse_message_panel").hide();
        }else{
            $("#reponse_panel").hide();
            $("#reponse_message_panel").show();
        }
    }
    //custom function
    displayResponse(txt, cancelFalse=true){
        var that = this;
        if(!txt){
            cancelFalse = false
        }
        txt = txt || "....";
        $(".cb__list").first().empty();
        if(txt.indexOf(".") == -1){
            that.pre_resp +=" "+ txt;
            $(".cb__list").first().html('<span class="bubble typing bubble_text">'+that.pre_resp+'</span>');
        }else{
            $(".cb__list").first().html('<span class="bubble typing bubble_text">'+txt+'</span>');
        }
        if(cancelFalse){
            var cancel_ = $("<button href='javascript:void(0)' class='butn butn-outline-primary' style='font-size: 10px;'> Cancel</button>");
            $(".cb__list").first().append(cancel_);
            cancel_.click(function(){
                that.processSystemResponse(that.conversation.current_response, true, true);
                that.onIdelState(true);
                that.conversation.abortReq();
                that.conversation.abortAsrReq();
            });
        }
    }
    postConversation(text, state_=null, title=null, query_type=null){
        super.postConversation(text, state_, title, query_type);
        if(!this.conversation.engagement_id){
            this.createSession();
        }
    }
}
