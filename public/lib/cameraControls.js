
class CameraControls {

	static cameraInit(elemntId = "") {

		CameraControls.screenHeight =  window.innerHeight;
		CameraControls.screenWidth = window.innerWidth;
		CameraControls.currentMediaStream = undefined;
		CameraControls.mobileDevice = false;
		CameraControls.camera_array = undefined;
		CameraControls.current_camera_index = undefined;
		CameraControls.currentStream = undefined;
		CameraControls.canvas = undefined;
		CameraControls.video = document.createElement("video");
		CameraControls.video.setAttribute("id", "vid");
		CameraControls.video.setAttribute("height", 450);
		CameraControls.video.setAttribute("width", 800);
		CameraControls.video.setAttribute("style", "display: none;")
		document.getElementsByTagName("body")[0].appendChild(CameraControls.video);
		// <video id = "vid" height="450" width="800" style="display: inline-block;"></video>

		console.log("initiating camera");
		CameraControls.video = document.getElementById(elemntId);
		navigator.mediaDevices.enumerateDevices().then(CameraControls.gotDevices).then(CameraControls.autoSelectCamera);
	}

	static stopMediaTracks(stream) {
		console.log("Stopping stream "+stream);
		stream.getTracks().forEach(track => {
			track.stop();
		});
	}

	static gotDevices(mediaDevices) {
		let count = 1;
		CameraControls.camera_array = [];
		mediaDevices.forEach(mediaDevice => {
			if (mediaDevice.kind === 'videoinput') {
				const option = document.createElement('option');
				option.value = mediaDevice.deviceId;
				CameraControls.camera_array.push(mediaDevice.deviceId);
				console.log("Camera "+`${count++}`,+" "+mediaDevice.deviceId);
				const label = mediaDevice.label || `Camera ${count++}`;
				const textNode = document.createTextNode(label);
				option.appendChild(textNode);
			}
		});
	}

	static autoSelectCamera() {
		CameraControls.selectCamera(1);
		CameraControls.current_camera_index=1;
	}

	static selectCamera(camera_array_index) {
		let deviceId = CameraControls.camera_array[camera_array_index];
		console.log("Selecting camera "+deviceId);
		if (typeof CameraControls.currentStream !== 'undefined') {
			console.log("Active media tracks detected. Stopping them.");
			CameraControls.stopMediaTracks(CameraControls.currentStream);
		} else {
			console.log("No active media tracks found.");
		}

		const videoConstraints = {};
		console.log("Using video source. "+deviceId);
		if (deviceId === '') {
			videoConstraints.facingMode = 'environment';
		} else {
			videoConstraints.deviceId = deviceId;
			videoConstraints.width = { min: 640, ideal: 1280, max: 1280 };
			videoConstraints.height = { min: 360, ideal: 720 };
		}


		const constraints = {
			video: videoConstraints,
			audio: false
		};
		console.log("Using Constraints "+JSON.stringify(constraints));
		navigator.mediaDevices.getUserMedia(constraints).then(stream => {
			CameraControls.currentMediaStream = CameraControls.currentStream = stream;
			CameraControls.video.html = '';
			CameraControls.video.srcObject = stream;
			CameraControls.video.play();
			return navigator.mediaDevices.enumerateDevices();
		}).then(CameraControls.gotDevices).catch(error => {
			console.error(error);
			// console.error(error.message);
			// console.error(error.stack);
		});
	}

	static captureImage(canvas = undefined) {
		console.log("Capturing the image.");
		if (!CameraControls.canvas) {
			CameraControls.canvas = document.getElementById("capturedImageCanvas");
			if (!CameraControls.canvas){
				CameraControls.canvas = document.createElement("canvas");
				CameraControls.canvas.setAttribute("id", "capturedImageCanvas");
				CameraControls.canvas.setAttribute("height", 720+"px");
				CameraControls.canvas.setAttribute("width", 1280+"px");
				CameraControls.canvas.style.objetFit = "contain";
				CameraControls.canvas.style.display = "none";
				document.getElementsByTagName("BODY")[0].appendChild(CameraControls.canvas);
				CameraControls.canvas = document.getElementById("capturedImageCanvas");
			}
		}
		CameraControls.canvas.getContext('2d').drawImage(CameraControls.video, 0, 0, CameraControls.canvas.width, CameraControls.canvas.height);
		let image = new Image();
		image = CameraControls.canvas.toDataURL('image/png');
		return image;
	}
}	


//Event Handlers


var toggleCameraHandler = function (event) {
	console.log("Button clicked. Switching video source.");
	console.log("Checking currentStream for active media tracks.");
	console.log("Current camera index is "+current_camera_index+", deviceId "+camera_array[current_camera_index]);
	if (CameraControls.current_camera_index + 1 > CameraControls.camera_array.length ) {
		console.log("Camera array overflow. Selecting 0, deviceId "+CameraControls.camera_array[0]);
		selectCamera(0);
		CameraControls.current_camera_index = 0;
	} else {
		console.log("Selecting "+(CameraControls.current_camera_index + 1)+", deviceId "+CameraControls.camera_array[CameraControls.current_camera_index + 1]);
		selectCamera(CameraControls.current_camera_index+1);
		CameraControls.current_camera_index = CameraControls.current_camera_index + 1;
	}    
};



//Get screen orientation.
function getScreenOrientation () {
	let orientation = window.orientation;
	let screenHeight = window.innerHeight;
	let screenWidth =  window.innerWidth;

	if ( orientation == 90 ) {
		return "landscape";
	} else if ( orientation == 0 ) {
		return "portrait";
	}
}

function getAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
	var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
	return { width: srcWidth*ratio, height: srcHeight*ratio };
}
