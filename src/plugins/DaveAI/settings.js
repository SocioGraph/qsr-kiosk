const settings = {
        enterprise_id: "dave_restaurant",
        conversation_id: "deployment_kiosk",
        event_type: "deployment",
        event_id: "kiosk",
        host_url: "https://staging.iamdave.ai",
        signup_apikey: "ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__",
        unity_url: "https://s3.ap-south-1.amazonaws.com/unity-plugin.iamdave.ai/unity/dev_empty_bg",
        type: "kiosk",
        default_placeholder: "Type here",
        recognizer_type: "google",
        session_model: "session",
        user_id_attr: "customer_id",
        interaction_model: "interaction",
        speech_recognition_url: "https://speech.iamdave.ai",
        image_recognition_url: "https://image.iamdave.ai",
        wakeup_url: "https://wakeup.iamdave.ai",
        avatar_id: "caitlyn",
        voice_id: "english-female",
        minimize: true,
        vad: true,
        additional_session_info: {
            deployment_scenario: "kiosk",
            conversation_id: "deployment_kiosk",
            device_id: "device1"
        },
        session_time: 50
    }
    //wakeup server 1. record voice and send backend 2. send a flag to backend to initiate the wake listener
    //image server 1. Capture video and send backend 2. send a flag to backend to initiate the image server
    //speech server 1. send flag backend to detect voice

export default settings;