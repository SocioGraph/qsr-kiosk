import eventTypes from './events.type'
import { has } from 'lodash'
import PluginSettings from './settings'
import store from '@/store'
import dave from '@/store/dave';
// daveobj.postConversation("cs_dish", <product_id>) // for viewing product
// daveobj.postConversation("cs_category", <category_id>) // for selecting category
// daveobj.postConversation("cs_addon", "cs_addon") // for getting list of addons
// daveobj.postConversation("cs_size", <selected size>) // for selecting the size
// daveobj.postConversation("cs_add",<product_id>)// for adding an item from cart
// daveobj.postConversation("cs_remove",<product_id>)// from removing an item from cart
// daveobj.postConversation("cs_review", "cs_review")
// daveobj.postConversation("cs_checkout", "cs_checkout")
// daveobj.postConversation("cs_trend", "cs_trend")
// daveobj.postConversation("cs_menu", "cs_menu")
// daveobj.postConversation("cs_popular", "cs_popular")
// daveobj.postConversation("cs_offer", "cs_offer")

var hotword_detected = false;

export default {


    install(Vue, { bus, store, routes }) {

        if (!bus) {
            throw new Error('EvenBus instance is not supplied to DaveAI Plugin')
        }

        const settings = {
            ...PluginSettings,
            conversationLoadedCallback: function(data) {
                bus.$emit(eventTypes.CONVERSATION_LOADED, data);
                dave.startWakeupStream(); //commented this
                dave.startImageStream(); //commented this

                console.log('started for hotword person detect')
            },

            session_expired_callback: function() {
                dave.updateNewUser();
                console.log("session expired")
                dave.loadAvatar()
                dave.startWakeupStream(); //commented this
                dave.startImageStream(); //commented this
                hotword_detected = false;
                Vue.prototype.router.push("/")
            },
            signup_params: function() {
                return {
                    person_type: "kiosk",
                    person_name: "guest",
                    email: "dinesh+" + makeid(8) + "@i2ce.in",
                    validated: true,
                    device_id: "device1"
                }
            },
            actionCallbacks: function(_data, action) {

                // On conversationLoadedCallback you need to call these functions
                // startWakeupStream()
                // startImageStream()

                // hotword/person_detected -- stop 
                // stopWakeupStream()
                // stopImageStream()

                // onIdelState
                // startWakeupStream()
                // -- if you detect hotword here you have to do startRecording

                //action hotword -- play init response -- change the avatar image to unity avatar
                //action person_detected -- play init response -- change the avatar image to unity avatar
                //dave.resume();
                //minimize: false in settings.js
                console.log('Callback Action', action);
                if (action == "signup") {
                    // alert("signup")
                    bus.$emit(eventTypes.IDLE, {})
                } else if (action == "hotword" || action == "person_detected") {
                    dave.stopWakeupStream();
                    dave.stopImageStream();
                    if (!hotword_detected) {
                        console.log('dave resume hotword')
                        dave.resume();
                        hotword_detected = true;
                    }
                }
                window.addEventListener("touchstart", function() {
                    dave.stopWakeupStream();
                    dave.stopImageStream();
                    if (!hotword_detected) {
                        console.log('dave resume hotword')
                        console.log('-------------------onclick session')
                        dave.resume();
                        hotword_detected =true;
                    }
                    
                });

                if (action != "conversation_response") {
                    return;
                }
                var data = _data["data"]
                console.log('Callback Response', data);

                if (_data['placeholder'] != '') {
                    store.dispatch('dave/avatartext', _data['placeholder']);
                }

                if (has(data, 'total_cart_items')) {
                    store.dispatch('app/counter', data.total_cart_items)
                }

                if (has(data, 'template_title')) {
                    if (data.template_title == "Checkout") {
                        store.dispatch('dave/showButtonStatus', 'scan-and-pay')
                        if (has(data, 'try_saying')) {
                            store.dispatch('dave/hints', data.try_saying)
                        }
                        // this.$bus.$emit('loaderStatus');
                        Vue.prototype.router.push({
                            name: 'ScanAndPay',
                        });
                    }
                }

                if (store.state.dave.quantityAddon) {
                    return;
                }

                if (has(data, 'template')) {
                    // Nested routes are not supported, not required for now.
                    const _route = routes.find(route => has(route, 'meta') && route.meta.template === data.template);
                    if (_route) {

                        if (has(data, 'category')) {
                            if (data.category.length != 0) {
                                store.dispatch('menu/updatecategory', data)
                                store.dispatch('dave/showButtonStatus', 'category')
                            }
                        }

                        const { meta } = _route
                        if (has(meta, 'action')) {
                            // Sending raw response, pick required data from store actions
                            store.dispatch(meta.action, data)
                        }

                        if (_route.name == "Cart") {
                            store.dispatch('dave/showButtonStatus', 'cart')
                            store.dispatch('dave/totalquantity', 0);
                        } else {
                            store.dispatch('dave/showButtonStatus', false)
                        }

                        if (_route.name == 'product.detail') {
                            store.dispatch('dave/productAddon', data);
                            store.dispatch('app/productAddonStatus', true);
                            store.dispatch('dave/showButtonStatus', 'product')
                        } else {
                            store.dispatch('app/productAddonStatus', false);
                            if (_route.name == 'Menu' && store.state.dave.menuStatus) {
                                store.dispatch('dave/showButtonStatus', 'menu')
                            }
                        }

                        if (!store.state.dave.menuStatus && _route.name == 'Menu') {
                            store.dispatch('dave/menuStatus', true);
                            store.dispatch('dave/showButtonStatus', 'cart')
                            bus.$emit(eventTypes.CONVERSATION, {
                                event: 'cs_show_cart',
                                value: 'cs_show_cart'
                            });
                            routes.push('/cart');
                            return;
                        }

                        // if ((store.state.dave.totalquantity != 0 || store.state.dave.totalquantity == 0) && _route.name == 'Menu' && store.state.dave.routehandle == 'Cart' && store.state.app.cart.total_amount != 0) {
                        //   alert("1")
                        //   bus.$emit(eventTypes.CONVERSATION, {
                        //     event: 'cs_show_cart',
                        //     value: 'cs_show_cart'
                        //   })
                        //   // store.dispatch('dave/routehandle',false);
                        //   routes.push('/cart');
                        //   store.dispatch('dave/showButtonStatus', 'cart')
                        //   return;
                        // }


                        if (_route.name != 'product.detail') {
                            if (store.state.dave.routehandle != _route.name) {
                                Vue.prototype.router.push({
                                    name: _route.name,
                                    params: {
                                        data: data
                                    }
                                }).catch(() => {});
                                store.dispatch('dave/routehandle', _route.name);
                            }
                            store.dispatch('dave/searchProcessingStatus', false);
                        }
                    }
                }

                if (has(data, 'try_saying')) {
                    store.dispatch('dave/hints', data.try_saying)
                }
            }
        }

        InteractionStageEnum.set("CLICK", "click_query");
        InteractionStageEnum.set("TEXT_INPUT", "typed_query");
        InteractionStageEnum.set("SPEECH_INPUT", "voice_query");
        InteractionStageEnum.set("MIC_ALLOWED", undefined);
        InteractionStageEnum.set("MIC_REJECTED", undefined);
        InteractionStageEnum.set("RESUMED", undefined);
        InteractionStageEnum.set("MINIMIZED", undefined);

        InteractionStageEnum.set("OPENED", undefined);
        InteractionStageEnum.set("AUTO_OPENED", undefined);
        InteractionStageEnum.set("MIC_REQUESTED", undefined);
        InteractionStageEnum.set("LEAVE_PAGE", undefined);

        Vue.prototype.$bus = bus
        const dave = new DaveAI(settings, bus)
        Vue.prototype.$daveai = dave

        const service = new DaveService(
            "https://staging.iamdave.ai",
            "dave_restaurant", {
                "user_id": "02c12d90-7890-36f5-b7aa-aab1ebd1496a",
                "api_key": "MDJjMTJkOTAtNzg5MC0zNmY1LWI3YWEtYWFiMWViZDE0OTZhMTY1Mjg1NTY5My4yNQ__",
                "authentication_cookie_key": "device-auth"
            }
        );
        // test server credentials for advert model
        // {"X-I2CE-USER-ID":"4b8130aa-56be-376d-a7a3-14fd5b654b06","X-I2CE-API-KEY":"NGI4MTMwYWEtNTZiZS0zNzZkLWE3YTMtMTRmZDViNjU0YjA2MTY0MzgxMTg2NS4xOQ__"}

        // staging server credentials for advert model
        // {"X-I2CE-USER-ID":"test_store_for_kiran_none","X-I2CE-API-KEY":"dGVzdF9zdG9yZV9mb3Jfa2lyYW5fbm9uZTE2MzUyMjQ1MTAuNDI_"}

        // "user_id": "02c12d90-7890-36f5-b7aa-aab1ebd1496a",
        // "api_key": "MDJjMTJkOTAtNzg5MC0zNmY1LWI3YWEtYWFiMWViZDE0OTZhMTY1Mjg1NTY5My4yNQ__",
        bus.$on(eventTypes.SESSION_EXPIRED, function() {
            dave.clearSession()
            // alert("clear")
        });

        bus.$on(eventTypes.CONVERSATION, function({ event, value }) {
            console.log({ event, value })
            if (event != '' || value != '') {
                dave.postConversation(value, event)
            }
            // dave.stopWakeupStream();
        });

        Vue.prototype.$service = service
    }
}

class DaveAI extends ConversationHandler {
    constructor(settings, bus) {
        super(settings);
        this.avatar_loaded = false;
        this.audios_list = [];
        this.$bus = bus
    }

    setUpConversationUI() {
        super.set("message_id", "message");
        super.set("quick_access_id", "quick_access");
        super.set("audio_id", "audio");

        this.audio = document.getElementById((this.settings["audio_id"] || "audio"));
        this.loadAvatar();
        this.setUpEvents();
    }

    setWhiteboard() {
        //TODO stub method
    }

    setPlaceholder() {
        //TODO stub method
    }
    micButtonClick() {
        var self = this;
        console.log('store.state.dave.searchProcessingStatus', store.state.dave.searchProcessingStatus)
            // if (store.state.dave.searchProcessingStatus) {
            //   return;
            // }
        if (this.load__) return
        store.dispatch('dave/updateListingTextState', false);
        store.dispatch('dave/searchProcessingStatus', false);
        store.dispatch('dave/updateListingState', true)
        console.log(self.state);
        if (self.state == "idel") {
            // alert("mic clicked")
            this.$bus.$emit(eventTypes.IDLE)
            self.conversation.startRecordResponse();
            self.state = "recording";
        } else if (self.state == "recording") {
            self.conversation.stopRecordResponse();
        } else {
            return
        }
    }
    setUpEvents() {
        var self = this;
        this.$bus.$on('__click', () => {
            self.micButtonClick()
        })
    }

    onLoadedCallback() {
        // TODO make the inteective ui visible
        if (this.conversation_loaded && this.avatar_loaded) {
            this.loaded = true
            this.conversationLoadedCallback(this.conversation.conversation_data);
            if (!this.minimize) {
                this.createSession()
                this.onResponse(this.conversation.init_response);
            } else {
                this.state = "pause";
            }
        }
    }

    loadAvatar() {
        this.$bus.$emit('unity_idel')
        var that = this;
        var t = SetupEnviron.load(this.settings["unity_url"], this.settings);
        t.then(function(ut) {
            that.avatar_loaded = true;
            that.unityInstance = ut;
            that.onLoadedCallback();
            setTimeout(() => {
                that.$bus.$emit('unity')
                store.dispatch("app/avataridelstatus", true);
            }, 5000);

        }, function(err) {
            console.error(err);
        });
        t.progress(function(pro) {
            console.log(pro);
        })
    }

    playAnimations(audo, force) {
        var that = this;
        if (!this.audios_list.length && !audo) return;
        if (!audo) {
            audo = this.audios_list.splice(0, 1);
            audo = audo[0];
        }
        this.unityInstance.SendMessage("head", "stopTalking");
        this.prev_audo = audo;
        this.temp_id = audo["id"];
        var tt = this.audio.cloneNode(true);
        var muted = this.audio.getAttribute("force-mute");
        this.audio.remove();
        this.audio = tt;
        this.audio.setAttribute("data-audio-id", that.temp_id);
        this.audio.setAttribute("force-mute", muted || false);
        this.audio.muted = muted || false;
        this.audio.src = audo["audio"];
        document.body.appendChild(this.audio);
        console.debug("play init id :: ", audo["id"]);
        this.audio.load();
        // this.audio.play();

        function _lis() {
            if (audo["id"] != that.temp_id && that.temp_id != that.audio.getAttribute("data-audio-id")) return;

            function playShapes_frames(id, frames, shapes) {
                if (that.forced_stop && that.forced_stop != that.audio.getAttribute("data-audio-id")) {
                    console.log("We have forced stopped, so don't play:" + id)
                    return;
                }
                if ((["speech-detected", "processing", "idel"].indexOf(that.state) < 0 && that.load__) && !force) {
                    console.log("State is not idle: " + that.state + " or we have loaded something else " + that.load__ + " already:" + id)
                    return;
                }
                if (!audo["defaults"]) {
                    that.onTalkingState();
                }
                if (!that.paused) {
                    that.unityInstance.SendMessage('head', 'StartTalking', JSON.stringify({
                        "frames": frames,
                        "shapes": shapes,
                        "audio_id": id
                    }))
                    store.dispatch("app/avataridelstatus", true);
                    that.audio_playing = true;
                    // that.conversation.hold(true);
                    that.playing_id = id;
                    console.debug("++++++Playing ID:" + id);
                } else {
                    console.debug("++++++Paused ID:" + id);
                }
                //$("#message").html(list[i][1]);
            }
            playShapes_frames(audo["id"], audo["frames_csv"], audo["shapes_csv"]);
        }
        this.audio.removeEventListener("canplaythrough", _lis, false);
        this.audio.addEventListener("canplaythrough", _lis, true);
        this.audio.onended = function() {
            that.audio_playing = false;
            if (that.playing_id != that.audio.getAttribute("data-audio-id")) return;
            that.unityInstance.SendMessage("head", "stopTalking");
            // that.conversation.hold(false);
            if (!that.conversation.processing) {
                that.onIdelState();
            }
            if (that.state == "idel" && !that.load__) {
                that.pre_resp = "";
            }
            if (audo["onEnds"]) {
                audo["onEnds"](that, that.playing_id);
            } else {
                that.playFromTheQueue(that, that.playing_id);
            }
        }
    }
    playDefaults(state, message, that) {
        super.playDefaults(state, message);

        // eslint-disable-next-line no-redeclare
        var that = that || this;

        function __load(level) {
            $.ajax({
                "url": that.host_url + "/defaults/" + that.voice_id + "/" + level + "/" + that.enterprise_id,
                "success": function(data) {
                    that.addAudioToQueue(data["audio"], state, "defaults", function(that, pid) {
                        that.playFromTheQueue(that, pid)
                        store.dispatch('dave/avatartext', data["subfolder"]);
                    }, null, null, data["subfolder"], false);
                }
            });
        }
        __load(state);
    }
    playFromTheQueue(that, playing_id) {
        that = that || this;
        if (that.audios_list.length) {
            that.playAnimations(null, false);
            return;
        }

        if (that._timer) {
            clearTimeout(that._timer);
        }
        if (playing_id != that.latest_audio_id) return;
        if (!that.play_followups.length) {
            return;
        }

        that._timer = setTimeout(function() {
            if (that.state != "idel" || that.load__) return;
            if (that.audios_list.length) {
                that.playAnimations(null, false);
            } else if (that.play_followups.length) {
                that.playNudge(that.play_followups[0], function(data) {
                    that.playNudge(data)
                });
                that.play_followups.splice(0, 1);
            }
        }, (that.conversation.current_response["wait"] || 10000));
    }
    addAudioToQueue(audio, id, conversation_id, onEnds, frames, shapes, subfolder, force) {
        var that = this;
        this.forced_stop = false;
        var id_ = id + "___" + makeid(4);
        console.debug("audio adding id :: ", id_);
        this.latest_audio_id = id_;
        if (force) {
            this.audios_list = [];
            //this.play_followups = [];
            /*if(this.audio){
                        this.audio.muted = true;
                        this.unityInstance.SendMessage("head", "stopTalking")
                    }*/
            this.forced_stop = id_;
            if (this._timer) {
                clearTimeout(this._timer);
            }
        }
        if (this._timer && onEnds) {
            clearTimeout(this._timer);
        }

        var aurl;
        if (audio.indexOf("http") >= 0) {
            aurl = audio;
        } else {
            aurl = this.host_url + "/" + audio;
        }
        conversation_id = conversation_id || this.conversation_id;
        var ob = {
            id: id_,
            audio: aurl,
            onEnds: onEnds,
            defaults: (conversation_id == "defaults")
        }
        this.audios_list.push(ob);

        var fil = "?voice_id=" + this.voice_id;
        if (subfolder)
            fil += "&subfolder=" + subfolder;


        function addFramesShapes(frames, shapes) {
            if (frames) {
                ob["frames_csv"] = frames;
            }
            if (shapes) {
                ob["shapes_csv"] = shapes;
            }
            if (ob["frames_csv"] && ob["shapes_csv"]) {
                if (!that.audio_playing) {
                    that.playAnimations(null, force);
                }
            }
        }

        var furl, surl;
        if (!frames) {
            furl = this.host_url + "/frames/" + conversation_id + "/" + id + "/" + this.enterprise_id + fil;
            $.ajax({
                "url": furl,
                "success": function(data) {
                    addFramesShapes(data)
                }
            })
        } else {
            addFramesShapes(frames);
        }
        if (!shapes) {
            surl = this.host_url + "/shapes/" + conversation_id + "/" + id + "/" + this.enterprise_id + fil;
            $.ajax({
                "url": surl,
                "success": function(data2) {
                    addFramesShapes(null, data2);
                }
            })
        } else {
            addFramesShapes(null, shapes);
        }
    }

    nudgeResponse(response) {
        super.nudgeResponse(response);
        var that = this;
        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
                // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
                if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                    this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function(that, pid) {
                        that.playFromTheQueue(that, pid)
                    }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
                } else {
                    this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function(that, pid) {
                        that.playFromTheQueue(that, pid)
                    }, null, null, null, true);
                }
            }
            //delete response["response_channels"];
        }
    }
    addNudges(lst) {
        this.play_followups = lst || [];
    }
    processSystemResponse(response, play_over) {

        super.processSystemResponse(response);
        this.state = "speaking";
        this.load__ = false
        var that = this;
        if (response["data"]) {
            if (response["data"]["avatar_model_name"] || response["data"]["avatar_id"]) {
                that.unityInstance.SendMessage("GameManagerOBJ", "showPersona", response["data"]["avatar_model_name"] || response["data"]["avatar_id"]);
                that.voice_id = response["data"]["voice_id"] || response["data"]["avatar_id"];
                // eslint-disable-next-line
                var defaul_avatar_id = response["data"]["avatar_model_name"] || response["data"]["avatar_id"]
            }
            if (response["data"]["bg"]) {
                that.unityInstance.SendMessage("GameManagerOBJ", "setTexture", response["data"]["bg"]);
                // eslint-disable-next-line
                var default_bg = response["data"]["bg"];
            }

            if (response["data"]["function"] && response["data"]["url"]) {
                that.unityInstance.SendMessage("GameManagerOBJ", response["data"]["function"], response["data"]["url"]);
            }
            if (response["data"]["redirect"]) {
                window.open(response["data"]["redirect"], '_blank');
            }
        }



        if (response["response_channels"]) {
            if (response["response_channels"]["shapes"] && response["response_channels"]["frames"]) {
                // this.addAudio(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/")[5], function(that, cno){ if(play_over){ play_over() }; that.playNext(that, cno) }, false, false, true);
                if (!response["response_channels"]["shapes"].endsWith(".csv")) {
                    this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["voice"].split("/").slice(-2, 1)[0], that.conversation_id, function(that, pid) {
                        if (play_over) {
                            play_over()
                        }
                        that.playFromTheQueue(that, pid)
                    }, response["response_channels"]["frames"], response["response_channels"]["shapes"], null, true);
                } else {
                    this.addAudioToQueue(response["response_channels"]["voice"], response["response_channels"]["frames"].split("/").splice(-2, 1)[0], that.conversation_id, function(that, pid) {
                        if (play_over) {
                            play_over()
                        }
                        that.playFromTheQueue(that, pid)
                    }, null, null, null, true);
                }
            }
            //delete response["response_channels"];
        }
    }

    onTalkingState() {
        // TODO disable the mic and freeze interection on talking state
        this.$bus.$emit(eventTypes.TALKING);
    }

    onIdelState(repeat) {
        // TODO disable the mic and freeze interection on talking state (invert)
        this.load__ = false;
        this.audio_playing = false;
        this.state = "idel";
        // $("#reponse_panel").show();
        
        this.$bus.$emit(eventTypes.IDLE);
        
        //start wakeup
        if (!repeat) {
            this.startWakeupStream(true);   //commented this
        }
        console.log('startWakeupStream on ideak state')
    }

    onProcessingRequest(_text) {
        // TODO animation for processing
        store.dispatch('dave/updateListingState', false);
        this.load__ = true
        console.log("Reached on processing request", _text);
        this.$bus.$emit(eventTypes.PROCESSING)
    }


    onRecording() {
        console.log("Started detecting");
    }
    onSpeechDetect(_text) {
        //TODO stub method       
        store.dispatch('dave/updateListingTextState', _text)
        if (_text == 'go back' || _text == 'goback') {
            this.$bus.$emit(eventTypes.CONVERSATION, {
                event: "",
                value: "go back",
            });
        }
        console.log("On speech detected state", _text)
    }
    postConversation(text, state_ = null, title = null, query_type = null) {
        super.postConversation(text, state_, title, query_type)
        if (!this.conversation.engagement_id) {
            this.createSession()
        }
    }
    pause() {
        this.audio_playing = false;
        this.mute();
        this.audio.src = undefined;
        this.unityInstance.SendMessage("head", "stopTalking");
        this.audios_list = [];
        this.onIdelState();
    }
    resume() {
        this.createSession()
        this.onResponse(this.conversation.init_response);
    }
    triggerEvents(action, data) {
        super.triggerEvents(action, data);
        console.log(" ****** ", action, this.state);
        
        if (action == "hotword") {
            if (this.state == "idel") {
                this.micButtonClick()
            }
            this.stopWakeupStream();
        }
    }
}