const events = {
  HOME: 'home',
  MENU: 'menu',
  CATEGORY: 'category',
  PRODUCT: 'product',
  CART: 'cart',
  QR_CODE: 'qr_code',
  REVIEW_ORDER: 'review_order',
  CHECKOUT: 'checkout'
}

export default events;
