import Vue from 'vue'
import Vuex from 'vuex'

import app from './app'
import menu from './menu'
import dave from './dave'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    menu,
    dave
  }
})
