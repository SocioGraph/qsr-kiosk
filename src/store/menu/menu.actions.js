import * as types from './menu.types'

export const updateMenu = ({ commit }, { menu }) => {
  commit(types.UPDATE_MENU, menu);
}

export const trending = ({ commit }, items) => {
  commit(types.TRENDING, items);
}

export const updatecategory = ({ commit }, category) => {
  commit(types.CATEGORY, category);
}
