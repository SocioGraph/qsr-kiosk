import * as getters from './menu.getters'
import mutations from './menu.mutation'
import * as actions from './menu.actions'

const state = {
  menu: [],
  trending: [],
  category: []
}

export default {
  namespaced: true,
  state,
  getters: getters,
  mutations: mutations,
  actions: actions
}