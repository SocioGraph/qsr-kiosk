import * as types from './menu.types'

export default {
  [types.UPDATE_MENU]: (state, menu) => {
    state.menu = menu
  },

  [types.TRENDING]: (state, items) => {
    state.trending = items
  },

  [types.CATEGORY]: (state, category) => {
    state.category = category
  },
}