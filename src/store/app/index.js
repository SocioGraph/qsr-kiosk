import * as getters from './getters'
import mutations from './mutation'
import * as actions from './actions'

const initialState = {
  cart: [],
  count: 0,
  avataridelstatus:false,
  productAddonStatus:false,
  homeactionstatus:false
}

export default {
  namespaced: true,
  state: initialState,
  getters: getters,
  mutations: mutations,
  actions: actions
}
