import * as types from './mutation-type'

export default {
  [types.ADD_TO_CART]: (state) => {
    state.cartItemsCount = state.cartItemsCount + 1
  },
  [types.CART]: (state, cart) => {
    state.cart = cart
  },
  [types.UPDATE_COUNT]: (state, count) => {
    state.count = count
  },
  [types.AVATARIDELSTATUS]: (state, avataridelstatus) => {
    state.avataridelstatus = avataridelstatus
  },
  [types.PRODUCTADDONSTATUS]: (state, value) => {
    state.productAddonStatus = value
  },
  [types.HOMEACTIONSTATUS]: (state, value) => {
    state.homeactionstatus = value
  }
}
