import * as types from './mutation-type'

export const addToCart = ({ commit }) => {
  commit(types.ADD_TO_CART);
}

export const cart = ({ commit }, { cart }) => {
  commit(types.CART, cart)
}

export const counter = ({ commit }, count) => {
  commit(types.UPDATE_COUNT, count)
}

export const avataridelstatus = ({ commit }, avataridelstatus) => {
  commit(types.AVATARIDELSTATUS, avataridelstatus)
}

export const productAddonStatus = ({ commit }, value) => {
  commit(types.PRODUCTADDONSTATUS, value);
}

export const homeactionstatus = ({ commit }, value) => {
  commit(types.HOMEACTIONSTATUS, value);
}
