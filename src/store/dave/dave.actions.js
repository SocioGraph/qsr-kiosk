import * as types from './dave.types'

export const hints = ({ commit }, hints) => {
  commit(types.UPDATE_HINTS, hints);
}

export const updateListingState = ({ commit }, value) => {
  commit(types.LISTING_STATE, value);
}

export const updateListingTextState = ({ commit }, value) => {
  commit(types.LISTING_TEXT_STATE, value);
}

export const showButtonStatus = ({ commit }, value) => {
  commit(types.SHOWBUTTONSTATUS, value);
}

export const productAddon = ({ commit }, value) => {
  commit(types.PRODUCTADDON, value);
}

export const quantityAddon = ({ commit }, value) => {
  commit(types.QUANTITYADDON, value);
}

export const totalquantity = ({ commit }, value) => {
  commit(types.TOTALQUANTITY, value);
}

export const routehandle = ({ commit }, value) => {
  commit(types.ROUTEHANDLE, value);
}

export const searchProcessingStatus = ({ commit }, value) => {
  commit(types.SEARCHPROCESSINGSTATUS, value);
}

export const menuStatus = ({ commit }, value) => {
  commit(types.MENUSTATUS, value);
}

export const idelpopupStatus = ({ commit }, value) => {
  commit(types.IDELPOPUPSTATUS, value);
}

export const avatartext = ({ commit }, value) => {
  commit(types.AVATARTEXT, value);
}