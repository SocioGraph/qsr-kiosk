import * as getters from './dave.getters'
import mutations from './dave.mutation'
import * as actions from './dave.actions'
import * as constants from './dave.constants'

const state = {
  hints: constants.hints,
  listingState: false,
  listingTextState: false,
  showButtonStatus: false,
  productAddonItems: [],
  quantityAddon:false,
  totalquantity:0,
  routehandle:false,
  searchProcessingStatus:false,
  menuStatus:true,
  idelpopupStatus:false,
  avatartext:false
}

export default {
  namespaced: true,
  state,
  getters: getters,
  mutations: mutations,
  actions: actions
}