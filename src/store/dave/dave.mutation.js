import * as types from './dave.types'

export default {
  [types.UPDATE_HINTS]: (state, hints) => {
    state.hints = hints
  },
  [types.LISTING_STATE]: (state, value) => {
    state.listingState = value
  },
  [types.LISTING_TEXT_STATE]: (state, value) => {
    state.listingTextState = value
  },
  [types.SHOWBUTTONSTATUS]: (state, value) => {
    state.showButtonStatus = value
  },
  [types.PRODUCTADDON]: (state, value) => {
    state.productAddonItems = value
  },
  [types.QUANTITYADDON]: (state, value) => {
    state.quantityAddon = value
  },
  [types.TOTALQUANTITY]: (state, value) => {
    state.totalquantity = value
  },
  [types.ROUTEHANDLE]: (state, value) => {
    state.routehandle = value
  },
  [types.SEARCHPROCESSINGSTATUS]: (state, value) => {
    state.searchProcessingStatus = value
  },
  [types.MENUSTATUS]: (state, value) => {
    state.menuStatus = value
  },
  [types.IDELPOPUPSTATUS]: (state, value) => {
    state.idelpopupStatus = value
  },
  [types.AVATARTEXT]: (state, value) => {
    state.avatartext = value
  }
}