import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueCarousel from 'vue-carousel'
import DaveAI from '@/plugins/DaveAI'
import { routes } from '@/router';
import IdleVue from "idle-vue";

Vue.use(VueCarousel)
export const bus = new Vue();
Vue.use(DaveAI, { bus, store, routes });

Vue.config.productionTip = false

const eventsHub = new Vue()
Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  store,
  idleTime: 30000, // 30 seconds
  startAtIdle: false
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
