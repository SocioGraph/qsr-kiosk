import Vue from 'vue'
import VueRouter from 'vue-router'
import Templates from '@/plugins/DaveAI/template.type'

Vue.use(VueRouter)

export const routes = [
  {
    path: '/',
    name: 'Home',
    exact: true,
    component: () => import('../views/Home.vue'),
    props: true,
    meta: {
      template: Templates.HOME,
    }
  },
  {
    path: '/cart',
    exact: true,
    name: 'Cart',
    component: () => import('../views/Cart.vue'),
    // props: {
    //   cartItems: [
    //     { imageName: '2.png', itemName:"Classic italian spaghetti", quantity:"2", price:"21.60", addsOn:"5.10", units:"$" },
    //     { imageName: '31.png', itemName:"Classic italian spaghetti", quantity:"2", price:"11.60", addsOn:"1.20", units:"$" },
    //   ],
    //   cartSuggestItems: [
    //     { imageName: '32.png', info:"Coca-Cola (250ML)", price:".30", units:"$" },
    //     { imageName: '33.png', info:"Fresh salads", price:".30", units:"$" },
    //     { imageName: '34.png', info:"Pecan Brownie", price:".30", units:"$" },
    //     { imageName: '35.png', info:"Steaks", price:".30", units:"$" },
    //   ],
    // }
    props: true,
    meta: {
      template: Templates.CART,
      action: 'app/cart'
    }
  },
  {
    path: '/review-order',
    exact: true,
    name: 'ReviewOrder',
    component: () => import('../views/ReviewOrder.vue'),
    // props: {
    //   cartItems: [
    //     { imageName: '2.png', itemName:"Classic italian spaghetti", quantity:"2", price:"21.60", addsOn:"5.10", units:"$" },
    //     { imageName: '31.png', itemName:"Classic italian spaghetti", quantity:"2", price:"11.60", addsOn:"1.20", units:"$" },
    //     { imageName: '32.png', itemName:"Coca Cola (1L)", quantity:"1", price:"10", addsOn:"0", units:"$" },
    //   ],
    //   units:'$'
    // }
    props: true,
    meta: {
      template: Templates.REVIEW_ORDER,
    }
  },
  {
    path: '/product',
    exact: true,
    name: 'product.detail',
    component: () => import('../views/Product.vue'),
    // props: {
    //   units:'$'
    // }
    props: true,
    meta: {
      template: Templates.PRODUCT,
    }
  },
  {
    path: '/category',
    exact: true,
    name: 'Category',
    component: () => import('../views/Category.vue'),
    // props: {
    //   pastaItems: [
    //     { filename: '2.png', title1:"Classic italian spaghetti", price:"108", units:"$" },
    //     { filename: '31.png', title1:"Classic spicey pasta", price:"108", units:"$" },
    //     { filename: '2.png', title1:"Classic italian spaghetti", price:"108", units:"$" },
    //     { filename: '31.png', title1:"Classic spicey pasta", price:"108", units:"$" },
    //     { filename: '31.png', title1:"Classic spicey pasta", price:"108", units:"$" },
    //     { filename: '2.png', title1:"Classic italian spaghetti", price:"108", units:"$" },
    //     { filename: '2.png', title1:"Classic italian spaghetti", price:"108", units:"$" },
    //     { filename: '31.png', title1:"Classic spicey pasta", price:"108", units:"$" },
    //   ],
    // }
    props: true,
    meta: {
      template: Templates.CATEGORY,
    }
  },
  {
    path: '/menu',
    exact: true,
    name: 'Menu',
    component: () => import('../views/Menu.vue'),
    props: true,
    meta: {
      template: Templates.MENU,
      action: 'menu/updateMenu'
    }
  },
  {
    path: '/scan-and-pay',
    exact: true,
    name: 'ScanAndPay',
    component: () => import('../views/ScanAndPay.vue'),
    props: true,
    meta: {
      template: Templates.QR_CODE,
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

Vue.prototype.router = router;
export default router
