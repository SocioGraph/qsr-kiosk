module.exports = {
	css: {
    loaderOptions: {
      scss: {
        prependData: `
			@import "@/assets/styles/scss/app.scss";
			@import "@/assets/styles/scss/views/Home.scss";
			@import "@/assets/styles/scss/components/Item.scss";
			@import "@/assets/styles/scss/views/Cart.scss";
			@import "@/assets/styles/scss/components/CartComponent.scss";
			@import "@/assets/styles/scss/components/CartSuggestComponent.scss";
			@import "@/assets/styles/scss/views/ReviewOrder.scss";
			@import "@/assets/styles/scss/views/OrderDetail.scss";
			@import "@/assets/styles/scss/views/ItalianClassicPastas.scss";
			@import "@/assets/styles/scss/components/PastaItem.scss";
			@import "@/assets/styles/scss/views/Menu.scss";
			@import "@/assets/styles/scss/components/MenuItem.scss";
			@import "@/assets/styles/scss/views/ScanAndPay.scss";
			@import "@/assets/styles/scss/components/Dave.scss";
			@import "@/assets/styles/scss/components/MenuSection.scss";
		`
      },
    },
  },
  configureWebpack: {
    externals: {
      ConversationHandler: {
        commonjs: 'ConversationHandler',
        amd: 'ConversationHandler',
        root: 'ConversationHandler'
      },
      SetupEnviron: {
        commonjs: 'SetupEnviron',
        amd: 'SetupEnviron',
        root: 'SetupEnviron'
      },
      DaveService: {
        commonjs: 'DaveService',
        amd: 'DaveService',
        root: 'DaveService'
      },
      InteractionStageEnum: {
        commonjs: 'InteractionStageEnum',
        amd: 'InteractionStageEnum',
        root: 'InteractionStageEnum'
      },
      makeid: {
        commonjs: 'makeid',
        amd: 'makeid',
        root: 'makeid'
      },
      $: {
        $: 'JQuery',
        amd: 'JQuery',
        root: 'JQuery'
      }
    }
  }
}